<?php
App::uses('ExceptionRenderer', 'Error');

class AppExceptionRenderer extends ExceptionRenderer {

    public function error400($error) {

        //begin redirect das urls antigas
       /* if($error->getCode() == 404){
            $url = $_SERVER['REQUEST_URI'];
            if(substr($_SERVER['REQUEST_URI'], 0, 1) == '/'){
                $url = substr($_SERVER['REQUEST_URI'], 1);
            }

            App::import('Model', 'FdRotas.Redirecionamento');
            $this->Redirecionamento = new Redirecionamento();
            $db = $this->Redirecionamento->find('first', array('conditions' => array('Redirecionamento.url_antiga' => $url)));
            if(!empty($db)){
             $this->controller->redirect('/'.$db['Redirecionamento']['url_nova'], $db['Redirecionamento']['tipo']);
            }else{
                if(stripos($url, '?') != false){
                    $url = substr($url, 0, stripos($url, '?'));

                    $db = $this->Redirecionamento->find('first', array('conditions' => array('Redirecionamento.url_antiga' => $url)));
                    if(!empty($db)){
                        $this->controller->redirect('/'.$db['Redirecionamento']['url_nova'], $db['Redirecionamento']['tipo']);
                    }else{
                        $url .= '/';
                        $db = $this->Redirecionamento->find('first', array('conditions' => array('Redirecionamento.url_antiga' => $url)));
                        if(!empty($db)){
                            $this->controller->redirect('/'.$db['Redirecionamento']['url_nova'], $db['Redirecionamento']['tipo']);
                        }
                    }
                }
            }
        }*/
        //end redirect das urls antigas

        $message = $error->getMessage();
        if (Configure::read('debug') == 0 && $error instanceof CakeException) {
            $message = __d('cake', 'Not Found');
        }
        $url = $this->controller->request->here();
        $this->controller->response->statusCode($error->getCode());
        $this->controller->set(array(
            'name' => $message,
            'url' => h($url),
            'error' => $error,
            '_serialize' => array('name', 'url')
        ));
        $this->_outputMessage('error400');
    }
}