<?php

    class FdUsuariosController extends FdUsuariosAppController
    {

        public $uses = array('FdUsuarios.Usuario', 'FdUsuarios.Grupo', 'FdUsuarios.PasswordReset', 'FdProjetos.Projeto', 'FdProjetos.ProjetoUsuario');

        public function beforeFilter()
        {
            parent::beforeFilter();

            App::import('Model', 'FdUsuarios.Usuario');
            $this->Usuario = new Usuario();
        }

        /**
         * fatorcms_login method
         *
         * @return void
         */
        public function fatorcms_login()
        {
            if ($this->Auth->user()) {
                $this->Session->setFlash('Você já está logado.', 'fatorcms_success');
                if (stripos($this->referer(), 'login') === false && $this->referer() != "/") {
                    $this->redirect($this->referer());
                } else {
                    $this->redirect('/fatorcms/projetos');
                }
            }

            $this->layout = 'login';
            if ($this->request->is('post')) {
                if ($this->Auth->login()) {
                    $this->Usuario->id = $this->Auth->user('id');
                    $this->Usuario->saveField('ultimo_acesso', date('Y-m-d H:i:s'));
                    $this->redirect('/fatorcms/projetos');
                } else {
                    $this->Session->setFlash('Usuário ou senha estão incorretos', 'fatorcms_danger', array(), 'auth');
                }
            }
        }

        /**
         * fatorcms_logout method
         *
         * @return void
         */
        public function fatorcms_logout()
        {
            return $this->redirect($this->Auth->logout());
        }

        /**
         * fatorcms_index method
         *
         * @return void
         */
        public function fatorcms_index($page = 1)
        {

            // Get user
            $user = $this->Auth->user();

            //busco os tipos
            $this->loadModel('FdUsuarios.Grupo');

            if ($user['grupo_id'] != $this->Grupo->get_group('SUPER_ADMIN')) {
                $grupos = $this->Grupo->find('list', array('fields' => array('Grupo.id', 'Grupo.nome'), 'conditions' => array('Grupo.status' => true, 'Grupo.id <>' => 4, 'Grupo.id <>' => $this->Grupo->get_group('SUPER_ADMIN')), 'order' => 'Grupo.nome ASC'));
            } else {
                $grupos = $this->Grupo->find('list', array('fields' => array('Grupo.id', 'Grupo.nome'), 'conditions' => array('Grupo.status' => true, 'Grupo.id <>' => 4), 'order' => 'Grupo.nome ASC'));
            }

            $this->set('grupos', $grupos);

            // Add filter
            $this->FilterResults->addFilters(
                array(
                    'filtro_nome' => array(
                        'Usuario.nome' => array(
                            'operator' => 'LIKE',
                            'value'    => array('before' => '%', 'after' => '%'),
                        ),
                    ),
                )
            );

            $this->FilterResults->addFilters(
                array(
                    'filtro_email' => array(
                        'Usuario.email' => array(
                            'operator' => 'LIKE',
                            'value'    => array('before' => '%', 'after' => '%'),
                        ),
                    ),
                )
            );

            $this->FilterResults->addFilters(
                array(
                    'filtro_grupo_id' => array(
                        'Usuario.grupo_id' => array('select' => $this->FilterResults->select('Grupo...', $grupos)),
                    ),
                )
            );

            $this->FilterResults->setPaginate('page', $page);

            // Paginate
            $options['conditions'] = $this->FilterResults->getConditions();

            // Não é admin? então não listo os administradores
            if ($user['grupo_id'] != $this->Grupo->get_group('ADMIN') && $user['grupo_id'] != $this->Grupo->get_group('SUPER_ADMIN')) {
                if (is_array($options['conditions'])) {
                    $options['conditions'] = array_merge($options['conditions'], array('Usuario.grupo_id <>' => $this->Grupo->get_group('ADMIN'), 'Usuario.grupo_id <>' => $this->Grupo->get_group('SUPER_ADMIN')));
                } else {
                    $options['conditions'] = array('Usuario.grupo_id <>' => $this->Grupo->get_group('ADMIN'), 'Usuario.grupo_id <>' => $this->Grupo->get_group('SUPER_ADMIN'));
                }
            } elseif ($user['grupo_id'] != $this->Grupo->get_group('SUPER_ADMIN')) {
                if (is_array($options['conditions'])) {
                    $options['conditions'] = array_merge($options['conditions'], array('Usuario.grupo_id <>' => $this->Grupo->get_group('SUPER_ADMIN')));
                } else {
                    $options['conditions'] = array('Usuario.grupo_id <>' => $this->Grupo->get_group('SUPER_ADMIN'));
                }
            }

            if (AuthComponent::user('constructor')) {
                $options['conditions'] += array('Usuario.usuario_id' => AuthComponent::user('id'));
            }

            $options['order'] = 'Usuario.id DESC';
            $this->paginate = $options;

            // Exportar?
            if (isset($this->params->params['named']['acao']) && $this->params->params['named']['acao'] == "exportar") {
                $this->Reports->xls($this->Usuario->find('all', array('conditions' => $options['conditions'], 'recursive' => -1, 'callbacks' => false)), 'Usuarios');
            }

            // Paginate
            $usuarios = $this->paginate();
            $this->set(compact('usuarios'));
        }


        /**
         * fatorcms_add method
         *
         * @return void
         */
        public function fatorcms_add()
        {
            // eh post?
            if ($this->request->is('post')) {
                $this->Usuario->create();
                if ($this->Usuario->save($this->request->data)) {
                    $id = $this->Usuario->getInsertID();
                    if (isset($this->request->data['Usuario']['enviar'])) {
                        $this->sendPassword($id);
                    }

                    if (isset($this->request->data['ProjetoUsuario'])) {
                        foreach($this->request->data['ProjetoUsuario']['projeto_id'] as $projeto) {
                            $this->ProjetoUsuario->usuarioProjeto($projeto, $this->Usuario->getLastInsertId());
                        }
                    }

                    $this->Session->setFlash('Registro salvo com sucesso.', 'fatorcms_success');
                    $this->redirect($this->referer());
                } else {
                    $this->Session->setFlash('O registro não pode ser salvo. Verifique os campos em destaque.', 'fatorcms_danger');
                }
            }

            // Set grupos
            // Get user logado
            $user = $this->Auth->user();


            // Não listo o grupo de admin se o usuario logado não for admin
            if ($user['grupo_id'] == $this->Grupo->get_group('ADMIN')) {
                $this->set('grupos', $this->Grupo->find('list', array('recursive' => -1, 'fields' => array('Grupo.id', 'Grupo.nome'), 'conditions' => array('Grupo.id <>' => 4, 'Grupo.id <>' => $this->Grupo->get_group('SUPER_ADMIN')))));
            } elseif ($user['grupo_id'] != $this->Grupo->get_group('SUPER_ADMIN') && $user['grupo_id'] != $this->Grupo->get_group('ADMIN')) {
                $this->set('grupos', $this->Grupo->find('list', array('recursive' => -1, 'fields' => array('Grupo.id', 'Grupo.nome'), 'conditions' => array('Grupo.id <>' => 4, 'Grupo.id <>' => $this->Grupo->get_group('ADMIN'), 'Grupo.id <>' => $this->Grupo->get_group('SUPER_ADMIN')))));
            } else {
                $this->set('grupos', $this->Grupo->find('list', array('recursive' => -1, 'fields' => array('Grupo.id', 'Grupo.nome'), 'conditions' => array('Grupo.id <>' => 4))));
            }

            if (AuthComponent::user('constructor')) {
                $this->set('grupos', $this->Grupo->find('list', array('recursive' => -1, 'fields' => array('Grupo.id', 'Grupo.nome'), 'conditions' => array('Grupo.id' => 3))));
                $this->set('projetos', $this->Projeto->find('list', array('recursive' => -1, 'fields' => array('Projeto.id', 'Projeto.nome'), 'conditions' => array('Projeto.usuario_id' => $this->Auth->user("id")))));
            }
        }

        /**
         * fatorcms_edit method
         *
         * @throws NotFoundException
         * @param string $id
         * @return void
         */
        public function fatorcms_edit($id = null)
        {
            if (AuthComponent::user('constructor')) {
                $owner = $this->Usuario->find('first',
                    array(
                        'recursive'  => -1,
                        'conditions' => array(
                            'Usuario.id'         => $id,
                            'Usuario.usuario_id' => AuthComponent::user('id')
                        )
                    )
                );
                if (!$owner) {
                    $this->Session->setFlash('Ops, registro não encontrado :(', 'fatorcms_danger');
                    $this->redirect('/fatorcms/usuarios');
                }
            }

            // Get user logado
            $user = $this->Auth->user();

            // Se o usuário a ser exibo é admin, e o usuario logado não for, não deixo entrar
            if (($user['grupo_id'] != $this->Grupo->get_group('ADMIN') && $user['grupo_id'] != $this->Grupo->get_group('SUPER_ADMIN')) && $user['id'] != $id) {
//                $this->Session->setFlash(__('Você não tem permissão para alterar esse registro'), 'fatorcms_danger');
//                $this->_redirectFilter();
            }

            if ($user['grupo_id'] != $this->Grupo->get_group('SUPER_ADMIN') && $user['id'] != $id) {
//                $this->Session->setFlash(__('Você não tem permissão para alterar esse registro'), 'fatorcms_danger');
//                $this->_redirectFilter();
            }


            $this->Usuario->id = $id;
            if (!$this->Usuario->exists()) {
                throw new NotFoundException('Registro inválido.');
            }
            if ($this->request->is('post') || $this->request->is('put')) {
                if ($this->Usuario->save($this->request->data)) {
                    // if ($this->request->data['Usuario']['enviar']){
                    //     $this->sendPassword($id);
                    // }
                    $this->Session->setFlash('Registro salvo com sucesso.', 'fatorcms_success');
                    //$this->redirect(array('action' => 'index'));
                    $this->_redirectFilter($this->Session->read('referer'));
                } else {
                    $this->Session->setFlash('O registro não pode ser salvo. Verifique os campos em destaque.', 'fatorcms_danger');
                }
            } else {
                $options = array('conditions' => array('Usuario.' . $this->Usuario->primaryKey => $id));
                $this->request->data = $this->Usuario->find('first', $options);
                $this->Session->write('referer', $this->referer());
            }

            // Set usuario
            $options = array('conditions' => array('Usuario.' . $this->Usuario->primaryKey => $id));
            $usuario = $this->Usuario->find('first', $options);
            $this->set(compact('usuario'));

            // Get user logado
            $user = $this->Auth->user();

            // // Se o usuário a ser exibo é admin, e o usuario logado não for, não deixo entrar
            // if($this->request->data['Usuario']['grupo_id'] == 1 && $user['grupo_id'] != 1){
            //     $this->Session->setFlash(__('Você não tem permissão para alterar esse registro'), 'fatorcms_danger');
            //     $this->_redirectFilter();
            // }

            // Set grupos
            // Não listo o grupo de admin se o usuario logado não for admin
            // $user['grupo_id'] = 1;
            if ($user['grupo_id'] == $this->Grupo->get_group('SUPER_ADMIN')) {
                $this->set('grupos', $this->Grupo->find('list', array('recursive' => -1, 'fields' => array('Grupo.id', 'Grupo.nome'), 'conditions' => array('Grupo.id <>' => $this->Grupo->get_group('SUPER_ADMIN')))));
            } elseif ($user['grupo_id'] != $this->Grupo->get_group('SUPER_ADMIN') && $user['grupo_id'] != $this->Grupo->get_group('ADMIN')) {
                $this->set('grupos', $this->Grupo->find('list', array('recursive' => -1, 'fields' => array('Grupo.id', 'Grupo.nome'), 'conditions' => array('Grupo.id <>' => $this->Grupo->get_group('ADMIN'), 'Grupo.id <>' => $this->Grupo->get_group('SUPER_ADMIN')))));
            } else {
                $this->set('grupos', $this->Grupo->find('list', array('recursive' => -1, 'fields' => array('Grupo.id', 'Grupo.nome'))));
            }

            if (AuthComponent::user('constructor')) {
                $this->set('grupos', $this->Grupo->find('list', array('recursive' => -1, 'fields' => array('Grupo.id', 'Grupo.nome'), 'conditions' => array('Grupo.id' => 3))));
                $this->set('projetos', $this->Projeto->find('list', array('recursive' => -1, 'fields' => array('Projeto.id', 'Projeto.nome'), 'conditions' => array('Projeto.usuario_id' => $this->Auth->user("id")))));
            }
        }


        /**
         * fatorcms_update_pass method
         *
         * @throws NotFoundException
         * @param string $id
         * @return void
         */
        public function fatorcms_update_pass($id = null)
        {

            // Get user logado
            $user = $this->Auth->user();

            $id = base64_decode($id);
            $this->Usuario->id = $id;
            if (!$this->Usuario->exists()) {
                throw new NotFoundException('Registro inválido.');
            }
            if ($this->request->is('post') || $this->request->is('put')) {
                $this->request->data['Usuario']['grupo_id'] = $user['grupo_id'];
                if ($this->Usuario->save($this->request->data, false)) {
                    $this->Session->setFlash('Registro salvo com sucesso.', 'fatorcms_success');
                    //$this->redirect(array('action' => 'index'));
                    $this->_redirectFilter($this->Session->read('referer'));
                } else {
                    $this->Session->setFlash('O registro não pode ser salvo. Verifique os campos em destaque.', 'fatorcms_danger');
                }
            } else {
                $options = array('conditions' => array('Usuario.' . $this->Usuario->primaryKey => $id));
                $this->request->data = $this->Usuario->find('first', $options);
                $this->Session->write('referer', $this->referer());
                unset($this->request->data['Usuario']['senha']);
            }

            // Set usuario
            $options = array('conditions' => array('Usuario.' . $this->Usuario->primaryKey => $id));
            $usuario = $this->Usuario->find('first', $options);
            $this->set(compact('usuario'));

            // Get user logado
            $user = $this->Auth->user();
        }

        /**
         * fatorcms_delete method
         *
         * @throws NotFoundException
         * @param string $id
         * @return void
         */
        public function fatorcms_delete($id = null)
        {
            if (!$this->request->is('get')) {
                throw new MethodNotAllowedException();
            }
            $this->Usuario->id = $id;
            if (!$this->Usuario->exists()) {
                throw new NotFoundException('Registro inválido.');
            }
            if ($this->Usuario->delete()) {
                $this->Session->setFlash('Registro deletado com sucesso.', 'fatorcms_success');
                // $this->redirect(array('action' => 'index'));
                $this->_redirectFilter($this->referer());
            }
            $this->Session->setFlash('O registro não pode ser removido.', 'fatorcms_warning');
            // $this->redirect(array('action' => 'index'));
            $this->_redirectFilter($this->referer());
        }

        /**
         * fatorcms_status method
         *
         * @return void
         */
        public function fatorcms_status()
        {
            if (!$this->request->is('post')) {
                throw new NotFoundException('Registro inválido.');
            }
            echo $this->_saveStatus('Usuario', $this->request->data['id'], $this->request->data['value']);
            die;
        }

        /**
         * sendPassword method
         *
         * @return void
         */
        public function sendPassword($id, $data)
        {
            App::uses('CakeEmail', 'Network/Email');
            App::uses('HtmlHelper', 'View/Helper');

            $this->Email = new CakeEmail('smtp');
            $this->Html = new HtmlHelper(new View());

            $senha = strtolower($this->Usuario->generatePAssword());

            $mensagem = 'Olá ' . $data['Usuario']['nome'] . ',' . "\r\n\r\n";
            $mensagem .= 'Estamos enviando para o seu e-mail, a senha do seu sistema Imóveis em Rede.' . "\r\n";
            $mensagem .= 'A senha é: ' . $senha . "\r\n\r\n";
            $mensagem .= 'Para acessar a Imóveis em Rede, acesse:' . $this->Html->url('/fatorcms', true) . "\r\n";
            $mensagem .= 'Qualquer dúvida, não deixe de entrar em contato conosco pelo email, sacimoveisemrede@gmail.com' . "\r\n\r\n";
            $mensagem .= 'Atenciosamente,' . "\r\n";
            $mensagem .= 'Equipe Imóveis em Rede';

            $this->Email->to($data['Usuario']['email']);
            $this->Email->replyTo('sacimoveisemrede@gmail.com');
            $this->Email->subject('Nova senha');
            $this->Email->send($mensagem);

            //$this->request->data['Usuario']['senha'] = $senha;
            $this->Usuario->id = $id;
            $this->Usuario->saveField('senha_resetar', '');
            $this->Usuario->saveField('senha', AuthComponent::password($senha));
        }

        private function resetEmail($data)
        {
            App::uses('CakeEmail', 'Network/Email');
            App::uses('HtmlHelper', 'View/Helper');

            $this->Email = new CakeEmail('smtp');
            $this->Html = new HtmlHelper(new View());

            $senha = $this->Usuario->generatePAssword();

            $mensagem = 'Olá ' . $data['Usuario']['nome'] . ',' . "\r\n\r\n";
            $mensagem .= 'Recebemos um pedido para resetar sua senha.' . "\r\n";
            $mensagem .= 'Clique nesse link: ' . $this->Html->url('/fatorcms/resetar/' . $senha, true) . "\r\n\r\n";
            $mensagem .= 'Você irá ser direcionado para o sistema da Imóveis em Rede e em seguida, colocando sua nova senha.';
            $mensagem .= 'Para acessar a Imóveis em rede, acesse:' . $this->Html->url('/fatorcms', true) . "\r\n";
            $mensagem .= 'Qualquer dúvida, não deixe de entrar em contato conosco pelo email, sacimoveisemrede@gmail.com' . "\r\n\r\n";
            $mensagem .= 'Atenciosamente,' . "\r\n";
            $mensagem .= 'Equipe Imóveis em Rede';

            $this->Email->to($data['Usuario']['email']);
            $this->Email->replyTo('sacimoveisemrede@gmail.com');
            $this->Email->subject('Resetar a senha');
            $this->Email->send($mensagem);

            //$this->request->data['Usuario']['senha'] = $senha;
            $this->Usuario->id = $data['Usuario']['id'];
            $this->Usuario->saveField('senha_resetar', $senha);
        }

        public function resetPassword()
        {
            if ($this->request->is('post')) {
                if (empty($this->request->data)) {
                    $this->PasswordReset->invalidate('email', 'Por favor, digite seu e-mail.');
                    $this->redirect($this->referer());
                }
                $usuario = $this->Usuario->findByEmail($this->request->data['PasswordReset']['email']);
                if ($usuario) {
                    $this->resetEmail($usuario);
                }
                $this->Session->setFlash('Você irá receber um e-mail com as instruções.', 'fatorcms_success');
                $this->redirect($this->referer());
            }
            $this->redirect($this->referer());
        }

        public function fatorcms_reset($token)
        {
            if (!empty($token)) {
                $usuario = $this->Usuario->findBySenhaResetar($token);
                if ($usuario) {
                    $this->sendPassword($usuario['Usuario']['id'], $usuario);
                    $this->Auth->login($usuario['Usuario']);
                    $this->Session->setFlash('Sua senha foi resetada e enviada para seu e-mail.', 'fatorcms_success');
                    $this->redirect('/fatorcms/projetos');
                }
            }
            $this->redirect('/');
        }

    }