<?php

    class FdUsuarioImoveisController extends FdUsuariosAppController
    {

        public $uses = array('FdUsuarios.UsuarioImovel');

        public function fatorcms_index()
        {
            $data = $this->UsuarioImovel->find('all',
                array(
                    'order' => 'id DESC'
                )
            );
            $fields = $this->UsuarioImovel->columnReadOtherColumn($this->UsuarioImovel->getColumnsWithout(), 'corretor_id', 'corretor_id_texto');
            $fields = $this->UsuarioImovel->columnReadOtherColumn($fields, 'construtora_id', 'construtora_id_texto');
            $this->set(compact('fields', 'data'));
        }

    }