<?php $this->Html->addCrumb('Usuários', array('action' => 'index')) ?>
<?php $this->Html->addCrumb('Alterar Senha') ?>

<h3>Alterar Senha</h3>

<div class="panel">
	<div class="panel-body">
		<?php echo $this->Form->create('Usuario', array('role' => 'form', 'class' => 'minimal', 'novalidate' => true)) ?>
			<?php echo $this->Form->input('id') ?>
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<?php echo $this->Form->input('senha_nova', array('label' => 'Senha de acesso', 'type'=>'password', 'div' => false, 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<?php echo $this->Form->input('confirma', array('label' => 'Confirmar senha de acesso', 'type' => 'password', 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
			<?php echo $this->Form->submit('Salvar', array('class' => 'btn btn-success', 'div' => false)) ?>
			<a href="<?php echo $this->Html->Url(array('action' => 'index')); ?>" class="btn btn-default">Cancelar</a>
		<?php echo $this->Form->end() ?>
	</div>
</div>