﻿	<?php echo $this->Form->create('', array('class' => 'form-signin')) ?>
        <h2 class="form-signin-heading"><?php echo $this->Html->image('fatorcms/mini-logo.png'); ?></h2>
        <div class="login-wrap">
            <div class="user-login-info">
            	<?php echo $this->Session->flash('auth', array('element' => 'fatorcms_danger')) ?>
                <?php echo $this->Session->flash(); ?>
            	<?php echo $this->Form->input('email', array('type' => 'text','class' => 'form-control', 'placeholder' => 'E-mail', 'autofocus', 'div' => false, 'label' => false)) ?>
            	<?php echo $this->Form->input('senha', array('class' => 'form-control', 'placeholder' => 'Senha', 'div' => false, 'label' => false, 'type' => 'password')) ?>
            </div>
               <a data-toggle="modal" class="btn btn-white btn-block" href="#myModal">
                   <span>
                       Esqueci minha senha
                   </span>
               </a>
            <button class="btn btn-lg btn-login btn-block" type="submit">Fazer login</button>

        </div>

      <?php echo $this->Form->end() ?>

<!-- Modal -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Esqueceu a senha?</h4>
          </div>
          <?php echo $this->Form->create('PasswordReset', array('url' => '/fatorcms/service/resetPassword')) ?>
          <div class="modal-body">
              <?php echo $this->Form->input('email', array('label' => false, 'placeholder' => 'Digite seu e-mail cadastrado', 'autocomplete' => 'off', 'class' => 'form-control placeholder-no-fix')) ?>
              <span class="text-info">
                 Após o envio do formulário, você irá receber um link para resetar a senha.
              </span>
          </div>
          <div class="modal-footer">
              <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
              <button class="btn btn-success" type="submit">Resetar senha</button>
          </div>
          <?php echo $this->Form->end() ?>
      </div>
  </div>
</div>
<!-- modal -->
