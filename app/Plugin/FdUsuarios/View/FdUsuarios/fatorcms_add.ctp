<?php if (AuthComponent::user('master')) { ?>
    <?php $this->Html->addCrumb('Usuários', array('action' => 'index')) ?>
    <?php $this->Html->addCrumb('Cadastrar Usuário') ?>

    <h3>Cadastrar Usuário</h3>
<?php } else { ?>
    <?php $this->Html->addCrumb('Imobiliárias', array('action' => 'index')) ?>
    <?php $this->Html->addCrumb('Cadastrar Imobiliária') ?>
    <h3>Cadastrar Imobiliária</h3>
<?php } ?>

<div class="panel">
	<div class="panel-body">
		<?php echo $this->Form->create('Usuario', array('role' => 'form', 'class' => 'minimal', 'novalidate' => true)) ?>
        <div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<label>Status</label>
						<div class="icheck">
							<?php echo $this->Form->input('status', array(
                                'type'      => 'radio',
                                'options'   => array(1 => 'Ativo', 0 => 'Inativo'),
                                'default'   => 1,
                                'legend'    => false,
                                'before'    => '<div class="radio">',
                                'after'     => '</div>',
                                'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<?php echo $this->Form->input('grupo_id', array('options' => $grupos, 'class' => 'form-control', 'default' => 5)) ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<?php echo $this->Form->input('nome', array('label' => 'Nome', 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
            <?php if (AuthComponent::user('constructor')) { ?>
                <div class="row">
                    <div class="col-md-6 col-lg-6">
                        <label for="">
                            Projetos (Opcional)
                        </label>
                        <div class="form-group">
                            <?php echo $this->Form->input('ProjetoUsuario.projeto_id.', array('options' =>  array('' => 'Selecione um projeto') + $projetos, 'default' => 0, 'class' => 'form-control', 'multiple')) ?>
                            <p class="text-danger">Caso a imobiliária não seja vinculada a nenhum projeto, ela não irá ver conteúdo no sistema.</p>
                        </div>
                    </div>
                </div>
            <?php } ?>

            <?php if (AuthComponent::user('master')) { ?>
            <div class="row" style="display: none;">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<?php echo $this->Form->input('construtora_limite', array('label' => 'Limite de Projetos', 'value' => 0, 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
            <div class="row" style="display: none;">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<?php echo $this->Form->input('corretor_limite', array('label' => 'Limite de corretores', 'value' => 0, 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
            <?php } ?>
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<?php echo $this->Form->input('email', array('label' => 'E-mail', 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
        <?php /*
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<div class="icheck">
							<div class="checkbox single-row">
								<?php echo $this->Form->input('enviar', array('label' => 'Gerar senha automática e enviar por e-mail', 'type' => 'checkbox')) ?>
							</div>
						</div>
					</div>
				</div>
			</div> */ ?>
        <div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<?php echo $this->Form->input('senha_nova', array('label' => 'Senha de acesso', 'type' => 'password', 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<?php echo $this->Form->input('confirma', array('label' => 'Confirmar senha de acesso', 'type' => 'password', 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
        <?php /*
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<?php echo $this->Form->input('telefone', array('label' => 'Telefone', 'class' => 'form-control')) ?>
					</div>
				</div>
			</div> */ ?>
        <?php echo $this->Form->submit('Salvar', array('class' => 'btn btn-success', 'div' => false)) ?>
        <a href="<?php echo $this->Html->Url(array('action' => 'index')); ?>"
           class="btn btn-default">Cancelar</a>
        <?php echo $this->Form->end() ?>
	</div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        var $limite = $('#UsuarioConstrutoraLimite');
        var $lcorretor = $('#UsuarioCorretorLimite');
        $('#UsuarioGrupoId').change(function () {
            if ($(this).val() == 3) {
                $limite.parents('.row').show();
                $lcorretor.parents('.row').show();
            } else {
                $limite.parents('.row').hide();
                $lcorretor.parents('.row').hide();
            }
        });
    });
</script>