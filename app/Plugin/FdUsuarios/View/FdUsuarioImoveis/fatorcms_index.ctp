<?php $this->Html->css('fatorcms/dataTables/datatables.min', array('block' => 'css')); ?>
<?php $this->Html->addCrumb('Corretor Imóvel') ?>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <table class="table table-striped table-bordered table-hover dataTables-example">
                    <thead>
                        <tr>
                            <?php foreach ($fields as $column) { ?>
                                <?php if (isset($column['comment']) && $column['comment'] != null) { ?>
                                    <th><?php echo $column['comment']; ?></th>
                                <?php } ?>
                            <?php } ?>
                            <th class="text-center">Opções</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (isset($data)) { ?>
                            <?php foreach ($data as $row) { ?>
                                <tr>
                                    <?php foreach ($row['UsuarioImovel'] as $key => $fake) { ?>
                                        <?php if (isset($fields[$key]['comment']) && $fields[$key]['comment'] != null) { ?>
                                            <?php if (!isset($fields[$key]['other'])) { ?>
                                                <td><?php echo $row['UsuarioImovel'][$key]; ?></td>
                                            <?php } else { ?>
                                                <td><?php echo $row['UsuarioImovel'][$fields[$key]['other']]; ?></td>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                    <td class="text-center">
                                        <a href="<?php echo $this->Html->Url('/fatorcms/imoveis/delete/' . $row['UsuarioImovel']['id']); ?>">
                                            <i class="glyphicon glyphicon-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php } ?>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </section>
    </div>
</div>
<?php echo $this->Html->script('fatorcms/dataTables/datatables.min', array('block' => 'script')); ?>
<?php echo $this->Html->script('fatorcms/imoveis/imovel', array('block' => 'script')); ?>
