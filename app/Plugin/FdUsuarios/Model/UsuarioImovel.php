<?php

    class UsuarioImovel extends FdUsuariosAppModel
    {

        public $useTable = 'usuario_imoveis';

        public $belongsTo = array(
            'Construtora' => array(
                'className'  => 'Usuario',
                'foreignKey' => 'construtora_id',
                'fields'     => array('Construtora.nome')
            ),
            'Corretor'    => array(
                'className'  => 'Usuario',
                'foreignKey' => 'corretor_id',
                'fields'     => array('Corretor.nome')
            )
        );

        public function afterFind($results, $primary = false)
        {
            foreach ($results as $key => $result) {
                if (isset($result['UsuarioImovel']['construtora_id']) && !empty($result['UsuarioImovel']['construtora_id'])) {
                    $results[$key]['UsuarioImovel']['construtora_id_texto'] = $result['Construtora']['nome'];
                } else {
                    $results[$key]['UsuarioImovel']['construtora_id_texto'] = '';
                }
                if (isset($result['UsuarioImovel']['corretor_id']) && !empty($result['UsuarioImovel']['corretor_id'])) {
                    $results[$key]['UsuarioImovel']['corretor_id_texto'] = $result['Corretor']['nome'];
                } else {
                    $results[$key]['UsuarioImovel']['corretor_id_texto'] = '';
                }
            }
            return $results;
        }

        public function getImoveisAcceso($id)
        {
            $acessos = array();
            $imoveis = $this->find('all', array('conditions' => array('UsuarioImovel.corretor_id' => $id)));
            foreach($imoveis as $imovel) {
                array_push($acessos, $imovel['UsuarioImovel']['imovel_id']);
            }
            return $acessos;
        }

    }