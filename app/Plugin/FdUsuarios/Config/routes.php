<?php

    Router::connect('/fatorcms/resetar/:id',
        array(
            'plugin'     => 'fd_usuarios',
            'controller' => 'fd_usuarios',
            'action'     => 'reset',
            'prefix'     => 'fatorcms',
            'fatorcms'   => true
        ),
        array(
            'pass' => array('id'),
            'id'   => '[a-zA-Z0-9]+'
        )
    );

    Router::connect('/fatorcms/service/resetPassword',
        array(
            'plugin'     => 'fd_usuarios',
            'controller' => 'fd_usuarios',
            'action'     => 'resetPassword'
        )
    );

    Router::connect("/fatorcms/usuarios", array('plugin' => 'fd_usuarios', 'controller' => 'fd_usuarios', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true));
    Router::connect("/fatorcms/usuarios/:action/*", array('plugin' => 'fd_usuarios', 'controller' => 'fd_usuarios', 'prefix' => 'fatorcms', 'fatorcms' => true));

    Router::connect("/fatorcms/grupos", array('plugin' => 'fd_usuarios', 'controller' => 'fd_grupos', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true));
    Router::connect("/fatorcms/grupos/:action/*", array('plugin' => 'fd_usuarios', 'controller' => 'fd_grupos', 'prefix' => 'fatorcms', 'fatorcms' => true));

    Router::connect("/fatorcms/corretores", array(
        'plugin'     => 'fd_usuarios',
        'controller' => 'fd_usuario_imoveis',
        'action'     => 'index',
        'prefix'     => 'fatorcms',
        'fatorcms'   => true
    ));