<?php
class MenuuComponent extends Component {

	private $menus_for_layout = array();

    public function carregaMenu($alias = array("admin")) {

		if(!in_array("admin", $alias) && Cache::read('menus_for_layout') != false){
			$this->menus_for_layout = Cache::read('menus_for_layout');
		}else{
			App::import('Model', 'FdMenus.Link');
			$this->Link = new Link();
			App::import('Model', 'FdMenus.Menu');
			$this->Menu = new Menu();

			$conditions = array();
			if(is_array($alias) && count($alias) > 0){
				foreach($alias as $ali){
					$conditions['or'][] = array('Menu.slug' => $ali);
				}
			}

			$menus = $this->Menu->find('list', array('fields' => array('slug'), 'conditions' => $conditions));

			if(in_array("admin", $alias)){
				$usuario = AuthComponent::user();
				$this->roleId = $usuario['grupo_id'];
			}else{
				//publico
				$this->roleId = 5;
			}

			foreach ($menus AS $menuAlias) {
				$menu = $this->Link->Menu->find('first', array(
					'conditions' => array(
						'Menu.status' => 1,
						'Menu.slug' => $menuAlias,
						'Menu.link_count >' => 0,
					),
					'cache' => array(
						'name' => 'menu_' . $menuAlias,
						'config' => 'menus',
					),
					'recursive' => '-1',
				));

				if (isset($menu['Menu']['id'])) {
					$this->menus_for_layout[$menuAlias] = array();
					$this->menus_for_layout[$menuAlias]['Menu'] = $menu['Menu'];
					$findOptions = array(
						'conditions' => array(
							'Link.menu_id' => $menu['Menu']['id'],
							'Link.status' => 1,
							'AND' => array(
								array(
									'OR' => array(
										'Link.visibility_grupos' => '',
										'Link.visibility_grupos LIKE' => '%"' . $this->roleId . '"%',
									),
								),
							),
						),
						'order' => array(
							'Link.parent_id' => 'ASC',
							'Link.lft' => 'ASC',
						),
						'recursive' => -1,
					);
					$links = $this->Link->find('threaded', $findOptions);
					$this->menus_for_layout[$menuAlias]['threaded'] = $links;
				}
			}

			if(!in_array("admin", $alias)){
				Cache::write('menus_for_layout', $this->menus_for_layout);
			}
		}

		return $this->menus_for_layout;

    }
}
?>