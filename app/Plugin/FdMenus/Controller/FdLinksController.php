<?php
App::uses('AppController', 'Controller');

/**
 * FdLinks Controller
 *
 * @property Link $Link
 */
class FdLinksController extends FdMenusAppController {

	public $uses = array('FdMenus.Link');

/**
 * fatorcms_index method
 *
 * @return void
 */
	public function fatorcms_index() {

		// Add filter
		$this->FilterResults->addFilters(
			array(
				'filter' => array(
					'OR' => array(
						'Link.title'     	=> array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' )),
						'Link.link' 		=> array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' )),
						'Link.description' 	=> array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' )),
					)
				),
				'filter_title' => array(
					'Link.title'  => array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' ))
				),
				'filter_link' => array(
					'Link.link'  => array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' ))
				),

			)
		);

		// Define conditions
		//$this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());
		$conditions = $this->FilterResults->getConditions();

		if (isset($this->params->named['menu'])) {
			$menuId = $this->params->named['menu'];
		}
		if (empty($menuId)) {
			$this->Session->setFlash(__('Paramêtros inválidos'), 'fatorcms_danger');
			$this->redirect(array(
				'controller' => 'fd_fd_menus',
				'action' => 'index',
			));
			return;
		}

		// Get user logado
		$user = $this->Auth->user();

		if($menuId == 1 && $user['grupo_id'] != $this->Grupo->get_group('SUPER_ADMIN')){
			$this->Session->setFlash(__('Você não tem permissão para alterar esse registro'), 'fatorcms_danger');
			if($this->Session->read('referer') && $this->Session->read('referer') != "/"){
				$this->redirect($this->Session->read('referer'));
			}else{
				$this->redirect(array('fatorcms' => true, 'controller' => 'fd_menus', 'action' => 'index'));
			}
		}

		$menu = $this->Link->Menu->findById($menuId);
		//não tem menu? sai daqui!
		if (!isset($menu['Menu']['id'])) {
			$this->redirect(array(
				'controller' => 'fd_menus',
				'action' => 'index',
			));
			return;
		}

		$this->set('title_for_layout', 'Links: Menu ' . $menu['Menu']['nome']);

		$this->Link->recursive = 0;
		$conditions['AND'] = array('Link.menu_id' => $menuId);
		$linksTree = $this->Link->generateTreeList($conditions);

		$linksStatusFind = $this->Link->find('all', array(
			'conditions' => array(
				'Link.menu_id' => $menuId,
			),
			'fields' => array(
				'Link.id',
				'Link.seo_url',
				'Link.visibility_grupos',
				'Link.status',
			),
		));

		if(count($linksStatusFind) > 0){
			foreach ($linksStatusFind as $key => $find) {
				$linksStatus[$find['Link']['id']]['seo_url'] = $find['Link']['seo_url'];
				$linksStatus[$find['Link']['id']]['visibility_grupos'] = $find['Link']['visibility_grupos'];
				$linksStatus[$find['Link']['id']]['status'] = $find['Link']['status'];
			}
		}

		//find groups
		$this->loadModel('FdUsuarios.Grupo');
		$grupos = $this->Grupo->find('list', array('fields' => array('Grupo.id', 'Grupo.nome'), 'conditions' => array('Grupo.status' => true), 'order' => 'Grupo.ordem ASC'));
		
		$this->set(compact('linksTree', 'linksStatus', 'menu', 'grupos'));
	}

/**
 * fatorcms_add method
 *
 * @return void
 */
	public function fatorcms_add() {
		if ($this->request->is('post')) {
			$this->Link->create();
			$this->request->data['Link']['visibility_grupos'] = $this->Link->encodeData($this->request->data['Link']['visibility_grupos']);
			$this->Link->Behaviors->attach('Tree', array(
				'scope' => array(
					'Link.menu_id' => $this->request->data['Link']['menu_id'],
				),
			));
			if ($this->Link->save($this->request->data)) {
				// $this->Link->recover();
				$this->_resetCaches();
				$this->Session->setFlash(__('Registro salvo com sucesso.'), 'fatorcms_success');
				$this->redirect(array('action' => 'index', 'menu' => $this->Link->field('menu_id')));
			} else {
				$this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'), 'fatorcms_danger');
				$this->request->data['Link']['visibility_grupos'] = $this->Link->decodeData($this->request->data['Link']['visibility_grupos']);
			}
		}

		//não tem menu? sai daqui!
		if (!isset($this->params->named['menu']) || $this->params->named['menu'] == "") {
			$this->Session->setFlash(__('Paramêtros inválidos'), 'fatorcms_danger');
			$this->redirect(array(
				'controller' => 'fd_fd_menus',
				'action' => 'index',
			));
			return;
		}

		// set variables for view
		$menus = $this->Link->Menu->find('list', array('fields' => array('id', 'nome')));

		$this->loadModel('FdUsuarios.Grupo');
		$grupos = $this->Grupo->find('list', array('fields' => array('id', 'nome')));

		// $this->loadModel('Pagina');
		// $paginas = $this->Pagina->find('list', array('fields' => array('id', 'nome'), 'conditions' => array('status' => true)));

		$menu = $this->Link->Menu->findById($this->params->named['menu']);
		$this->set(compact('menu'));

		$parentIds = $this->Link->generateTreeList(array(
			'Link.menu_id' => $menu['Menu']['id']
		));
		$menuId = $menu['Menu']['id'];
		$this->set(compact('menus', 'grupos', 'parentIds', 'menuId', 'paginas'));
	}

/**
 * fatorcms_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function fatorcms_edit($id = null) {

		if (!$this->Link->exists($id)) {
			throw new NotFoundException(__('Link Inválido.'));
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			$this->request->data['Link']['visibility_grupos'] = $this->Link->encodeData($this->request->data['Link']['visibility_grupos']);
			$this->Link->Behaviors->attach('Tree', array(
				'scope' => array(
					'Link.menu_id' => $this->request->data['Link']['menu_id'],
				),
			));
			if ($this->Link->save($this->request->data)) {
				// $this->Link->recover();
				$this->_resetCaches();
				$this->Session->setFlash(__('Registro salvo com sucesso.'), 'fatorcms_success');
				$this->redirect(array('action' => 'index', 'menu' => $this->Link->field('menu_id')));
			} else {
				$this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'), 'fatorcms_danger');
			}
		} else {
			$options = array('conditions' => array('Link.' . $this->Link->primaryKey => $id));
			$this->request->data = $this->Link->find('first', $options);

			$this->request->data['Link']['visibility_grupos'] = $this->Link->decodeData($this->request->data['Link']['visibility_grupos']);
		}

		$options = array('conditions' => array('Link.' . $this->Link->primaryKey => $id));
		$link = $this->Link->find('first', $options);
		$this->set(compact('link'));

		//não tem menu? sai daqui!
		if (!isset($this->params->named['menu']) || $this->params->named['menu'] == "") {
			$this->Session->setFlash(__('Paramêtros inválidos'), 'fatorcms_danger');
			$this->redirect(array(
				'controller' => 'fd_menus',
				'action' => 'index',
			));
			return;
		}

		// Set variables for view
		$menus = $this->Link->Menu->find('list', array('fields' => array('id', 'nome')));
		$this->loadModel('Grupo');
		$grupos = $this->Grupo->find('list', array('fields' => array('id', 'nome')));

		// $this->loadModel('Pagina');
		// $paginas = $this->Pagina->find('list', array('fields' => array('id', 'nome'), 'conditions' => array('status' => true)));

		$menu = $this->Link->Menu->findById($this->params->named['menu']);
		$this->set(compact('menu'));

		$parentIds = $this->Link->generateTreeList(
			array('AND' => array(
			'Link.menu_id' => $menu['Menu']['id'],
			'Link.id <>' => $id
			))
		);

		$menuId = $menu['Menu']['id'];
		$this->set(compact('menus', 'grupos', 'parentIds', 'menuId', 'paginas'));

		// addBreadcrumb for view
		$this->_addBreadcrumb('Menus', array('fatorcms' => true, 'controller' => 'fd_menus', 'action' => 'index'));
		$this->_addBreadcrumb('Links', array('fatorcms' => true, 'controller' => 'links', 'action' => 'index', 'menu' => $menuId));
		$this->_addBreadcrumb($menu['Menu']['nome'], array('fatorcms' => true, 'controller' => 'links', 'action' => 'index', 'menu' => $menuId));
		$this->_addBreadcrumb('Editar Link');
	}

/**
 * fatorcms_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function fatorcms_delete($id = null) {
		$this->Link->id = $id;
		if (!$this->Link->exists()) {
			throw new NotFoundException(__('Link Inválido.'));
		}
		$menu_id = $this->Link->field('menu_id');
		// $this->request->onlyAllow('post', 'delete');
		$this->Link->Behaviors->attach('Tree', array(
			'scope' => array(
				'Link.menu_id' => $menu_id,
			),
		));
		if ($this->Link->delete()) {
			$this->_resetCaches();
			$this->Session->setFlash(__('Registro deletado.'), 'fatorcms_success');
			$this->redirect(array('action' => 'index', 'menu' => $menu_id));
		}
		$this->Session->setFlash(__('Registro não pode ser deletado.'), 'fatorcms_danger');
		$this->redirect(array('action' => 'index', 'menu' => $menu_id));
	}

/**
 * Admin moveup
 *
 * @param integer $id
 * @param integer $step
 * @return void
 * @access public
 */
	public function fatorcms_moveup($id, $step = 1) {
		$link = $this->Link->findById($id);
		if (!isset($link['Link']['id'])) {
			$this->Session->setFlash(__('Link Inválido.'), 'fatorcms_danger');
			$this->redirect(array(
				'controller' => 'fd_menus',
				'action' => 'index',
			));
		}
		$this->Link->Behaviors->attach('Tree', array(
			'scope' => array(
				'Link.menu_id' => $link['Link']['menu_id'],
			),
		));
		if ($this->Link->moveUp($id, $step)) {
			$this->_resetCaches();
			$this->Session->setFlash(__('Registro salvo com sucesso.'), 'fatorcms_success');
		} else {
			$this->Session->setFlash(__('O registro não pode ser salvo.'), 'fatorcms_danger');
		}
		$this->redirect(array(
			'action' => 'index',
			'menu' => $link['Link']['menu_id']
		));
	}

/**
 * Admin movedown
 *
 * @param integer $id
 * @param integer $step
 * @return void
 * @access public
 */
	public function fatorcms_movedown($id, $step = 1) {
		$link = $this->Link->findById($id);
		if (!isset($link['Link']['id'])) {
			$this->Session->setFlash(__('Link Inválido.'), 'fatorcms_danger');
			$this->redirect(array(
				'controller' => 'fd_menus',
				'action' => 'index',
			));
		}
		$this->Link->Behaviors->attach('Tree', array(
			'scope' => array(
				'Link.menu_id' => $link['Link']['menu_id'],
			),
		));
		if ($this->Link->moveDown($id, $step)) {
			$this->_resetCaches();
			$this->Session->setFlash(__('Registro salvo com sucesso.'), 'fatorcms_success');
		} else {
			$this->Session->setFlash(__('O registro não pode ser salvo.'), 'fatorcms_danger');
		}
		$this->redirect(array(
			'action' => 'index',
			'menu' => $link['Link']['menu_id']
		));
	}

/**
 * fatorcms_status method
 *
 * @return void
 */
    public function fatorcms_status(){
        if (!$this->request->is('post')){
            throw new NotFoundException('Registro inválido.');
        }
        echo $this->_saveStatus('Link', $this->request->data['id'], $this->request->data['value']);
        $this->_resetCaches();
        die;
    }
}
