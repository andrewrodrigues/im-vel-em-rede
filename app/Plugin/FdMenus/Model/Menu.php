<?php
    App::uses('AppModel', 'Model');
    App::uses('CakeSession', 'Model/Datasource');

    class Menu extends FdMenusAppModel
    {

        /**
         * Validation rules
         *
         * @var array
         */
        public $validate = array(
            'nome' => array(
                'notempty' => array(
                    'rule'    => array('notempty'),
                    'message' => 'Campo de preenchimento obrigatório.',
                ),
            ),
            'slug' => array(
                'notempty' => array(
                    'rule'    => array('notempty'),
                    'message' => 'Campo de preenchimento obrigatório.',
                ),
            ),
        );

        /**
         * hasMany associations
         *
         * @var array
         */
        public $hasMany = array(
            'Link' => array(
                'className'    => 'Link',
                'foreignKey'   => 'menu_id',
                'dependent'    => false,
                'conditions'   => '',
                'fields'       => '',
                'order'        => 'Link.lft ASC',
                'limit'        => '',
                'offset'       => '',
                'exclusive'    => '',
                'finderQuery'  => '',
                'counterQuery' => '',
            ),
        );
    }