<?php

/**
 * Link Model
 *
 */
class Link extends FdMenusAppModel {

	public $actsAs = array(
		'Containable',
        'Encoder',
        'Tree',
        'Cached' => array(
            'prefix' => array(
                'link_',
                'menu_',
            ),
        ),
        'Upload.Upload' => array(
            'thumb_file' => array(
                'fields' => array(
                    'dir' => 'thumb_dir',
                    'type' => 'thumb_type',
                    'size' => 'thumb_size',
                ),
                'thumbnailMethod' => 'php',
                'thumbnailSizes' => array(
                    'thumb' => '100x100'
                )
            )
        )
    );

/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'title' => array(
            'rule' => array('notempty'),
            'message' => 'Campo de preenchimento obrigatório.',
        ),
        'seo_url' => array(
            'rule' => array('validaLink'),
            'message' => 'Campo de preenchimento obrigatório.',
        ),
    );

/**
 * belongsTo associations
 *
 * @var array
 */
    public $belongsTo = array(
        'Menu' => array('counterCache' => true)
    );

	public function validaLink($data){
		if (isset($this->data[$this->alias]['pagina_id']) || isset($this->data[$this->alias]['categoria_id'])) {
			return true;
		}else if(strlen(reset($data)) > 0){
			return true;
		}
		return false;
	}

	public function beforeSave($options = array()) {
		// //obtem e seta a url da pagina
		// if (isset($this->data[$this->alias]['pagina_id']) && $this->data[$this->alias]['pagina_id'] != "" && $this->data[$this->alias]['tipo'] == "PAGINA") {
		// 	$model = ClassRegistry::init('Pagina');
		// 	$pagina = $model->find('first', array('fields' => array('Pagina.seo_url'), 'conditions' => array('Pagina.id' => $this->data[$this->alias]['pagina_id'])));

		// 	$this->data[$this->alias]['link'] = '/'.$pagina['Pagina']['seo_url'];
  //       }
		parent::beforeSave($options = array());
	}

}
?>