<?php $this->Html->addCrumb('Menus', array('controller' => 'fd_menus', 'action' => 'index')); ?>
<?php $this->Html->addCrumb('Links', array('action' => 'index', 'menu' => $menu['Menu']['id'])) ?>
<?php $this->Html->addCrumb($menu['Menu']['nome']); ?>

<div class="row">
    <h3 class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
        Menus
        <?php echo $this->Html->link('Cadastrar Link', array('action' => 'add', 'menu' => $this->params->named['menu']), array('class' => 'btn btn-info pull-right')); ?>
    </h3>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <table class="table table-hover general-table">
                    <thead>
                        <tr>
                            <th style="width:60px">#</th>
                            <th>Título</th>
                            <th style="width:280px">URL</th>
                            <th>Grupos</th>
                            <th class="text-center">Status</th>
                            <th class="text-center" style="width:280px">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($linksTree) > 0): ?>
                            <?php foreach($linksTree as $linkId => $linkTree): ?>
                            <tr>
                                <td><?php echo h($linkId); ?>&nbsp;</td>
                                <td><?php echo h($linkTree); ?>&nbsp;</td>
                                <td style="width:510px; display: block;word-wrap:break-word;"><?php echo h($linksStatus[$linkId]['seo_url']); ?>&nbsp;</td>
                                <td>
                                    <?php 
                                        if($linksStatus[$linkId]['visibility_grupos'] != ""):
                                            $grps = json_decode($linksStatus[$linkId]['visibility_grupos'], true);
                                            foreach ($grps as $key => $grupo): 
                                    ?>
                                            - <?php echo $grupos[$grupo]; ?>; <br />
                                    <?php          
                                            endforeach;
                                        endIf;
                                    ?>&nbsp;
                                </td>
                                <td>
                                    <input type="checkbox" class="atualiza-status" value="<?php echo $linksStatus[$linkId]['status'] == 1 ? 0 : 1 ?>" data-id="<?php echo $linkId ?>" data-url="<?php echo $this->Html->url(array('action' => 'status')) ?>" data-on-text="Sim" data-on-color="success" data-off-text="Não" data-off-color="danger"<?php echo $linksStatus[$linkId]['status'] == 1 ? ' checked="checked"' : '' ?>>
                                </td>
                                <td>
                                    <?php echo $this->Html->link('<i class="fa fa-arrow-up bigger-130"></i>', array('action' => 'moveup',  $linkId), array('class' => 'btn btn-default', 'escape' => false, 'data-rel' => 'tooltip', 'title' => 'Mover para cima')); ?>

                                    <?php echo $this->Html->link('<i class="fa fa-arrow-down bigger-130"></i>', array('action' => 'movedown', $linkId), array('class' => 'btn btn-default', 'escape' => false, 'data-rel' => 'tooltip', 'title' => 'Mover para baixo')); ?>

                                    <?php echo $this->Html->link('<i class="fa fa-edit"></i> Editar', array('action' => 'edit', $linkId, 'menu' => $this->params->named['menu']), array('class' => 'btn btn-primary btn-sm', 'escape' => false)) ?> 
                                    <?php echo $this->Html->link('<i class="fa fa-trash-o"></i> Remover', array('action' => 'delete', $linkId), array('class' => 'btn btn-danger btn-sm', 'escape' => false), __('Deseja mesmo remover o registro # %s?', $linkTree)) ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="5" class="text-center">Nenhum registro encontrado.</td>
                            </tr>
                        <?php endIf; ?>
                    </tbody>
                </table>
            </div>
        </section>
    </div>
</div>