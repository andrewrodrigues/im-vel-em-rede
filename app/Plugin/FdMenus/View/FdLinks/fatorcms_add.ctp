<?php $this->Html->addCrumb('Menus', array('controller' => 'fd_menus', 'action' => 'index')) ?>
<?php $this->Html->addCrumb('Links', array('action' => 'index', 'menu' => $menu['Menu']['id'])) ?>
<?php $this->Html->addCrumb($menu['Menu']['nome'], array('action' => 'index', 'menu' => $menuId)) ?>
<?php $this->Html->addCrumb('Adicionar Link') ?>

<h3>Cadastrar Link no Menu: <?php echo $menu['Menu']['nome']; ?></h3>

<div class="panel">
	<div class="panel-body">
	<?php echo $this->Form->create('Link', array('type' => 'file', 'role' => 'form', 'class' => 'minimal', 'novalidate' => true)) ?>
		<?php echo $this->Form->input('id') ?>
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group">
					<label>Status</label>
					<div class="icheck">
						<?php echo $this->Form->input('status', array(
																	'type' => 'radio',
																	'options' => array(1 => 'Ativo', 0 => 'Inativo'), 
																	'default' => 1,
																	'legend' => false,
																	'before' => '<div class="radio">', 
																	'after' => '</div>',
																	'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<?php echo $this->Form->input('title', array('label' => 'Título', 'class' => 'form-control')) ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<?php echo $this->Form->input('seo_url', array('label' => 'URL', 'class' => 'form-control')) ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4">
				<div class="form-group">
					<?php echo $this->Form->input('menu_id', array('options' => $menus, 'value' => $menuId, 'class' => 'form-control input-sm m-bot15')); ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4">
				<div class="form-group">
					<?php echo $this->Form->input('parent_id', array('options' => $parentIds, 'empty' => true, 'class' => 'form-control input-sm m-bot15')); ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<?php echo $this->Form->input('visibility_grupos', array('label' => 'Visível', 'options' => $grupos, 'multiple' => true, 'class' => 'form-control')); ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group">
					<label>Status</label>
					<div class="icheck">
						<?php echo $this->Form->input('target', array(
																	'type' => 'radio',
																	'options' 	=> array(
																		'_self' => '_self',
																		'_blank' => '_blank'
																	),
																	'default' => '_self',
																	'legend' => false,
																	'before' => '<div class="radio">', 
																	'after' => '</div>',
																	'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
					</div>
				</div>
			</div>
		</div>
		<?php echo $this->Form->submit('Salvar', array('class' => 'btn btn-success', 'div' => false)) ?>

		<a href="<?php echo $this->Html->Url(array('action' => 'index', 'menu' => $this->params->named['menu'])); ?>" class="btn btn-default">&nbsp;Cancelar</a>
	<?php echo $this->Form->end() ?>
	</div>
</div>