<?php

class AclConfigComponent extends Component
{
	var $components = array('Auth', 'Acl', 'Session', 'Common');

	public function initialize(Controller $controller){
	    $this->controller = $controller;
	}

   	public function configureAcl(){
        //Configure AuthComponent
        $this->Auth->authenticate = array(
            AuthComponent::ALL => array(
                                        'userModel' => 'FdUsuarios.Usuario',
                                        'fields'    => array('username' => 'email', 'password' => 'senha'),
                                        'scope'     => array('Usuario.status' => true)
                                    ),
            'Form'
        );

        $this->Auth->authorize = array(
            'Actions' => array(
                'actionPath' => 'controllers',
                'userModel' => 'FdUsuarios.Usuario',
            ),
        );

        //Definicoes do methods de autenticação
        if($this->Common->isAdminMode()){
            $this->Auth->autoRedirect = true;
            $this->Auth->loginAction = array('fatorcms' => true, 'plugin' => 'fd_usuarios', 'controller' => 'fd_usuarios', 'action' => 'login');
            $this->Auth->loginRedirect = array('fatorcms' => true, 'plugin' => 'fd_dashboard', 'controller' => 'fd_dashboard', 'action' => 'index');
            $this->Auth->logoutRedirect = array('fatorcms' => true, 'plugin' => 'fd_usuarios', 'controller' => 'fd_usuarios', 'action' => 'login', 'prefix' => 'fatorcms');
            $this->Auth->authError = 'Efetue login com permissão nessa sessão para prosseguir.';
        }else{
            $this->Auth->autoRedirect = false;
            $this->Auth->loginAction = array('fatorcms' => true, 'plugin' => 'fd_usuarios', 'controller' => 'fd_usuarios', 'action' => 'login');
            $this->Auth->loginRedirect = array('fatorcms' => false, 'plugin' => 'fd_usuarios', 'controller' => 'home', 'action' => 'index');
            $this->Auth->logoutRedirect = array('fatorcms' => false, 'plugin' => 'fd_usuarios', 'controller' => 'home', 'action' => 'index');
            $this->Auth->authError = 'Faça login para prosseguir.';
        }

        //Begin permissons
        if ($this->Auth->user() && $this->Auth->user('grupo_id') == 1) {
            // Role: Admin
            $this->Auth->allowedActions = array('*');
        } else {
            if ($this->Auth->user()) {
                $roleId = $this->Auth->user('grupo_id');
            }else{
                $roleId = 6; // Role: Public
            }

            // $thisControllerNode = $this->Acl->Aco->node($this->Auth->actionPath . $this->name);
            $thisControllerNode = $this->Acl->Aco->find('all', array('order' => 'rght asc', 'conditions' => array('alias' => $this->Auth->actionPath . $this->controller->name)));

            if ($thisControllerNode) {
                $thisControllerNode = $thisControllerNode['0'];

                $thisControllerActions = $this->Acl->Aco->find('list', array(
                            'conditions' => array(
                                'Aco.parent_id' => $thisControllerNode['Aco']['id'],
                            ),
                            'fields' => array(
                                'Aco.id',
                                'Aco.alias',
                            ),
                            'recursive' => '-1',
                        ));

                $thisControllerActionsIds = array_keys($thisControllerActions);


                $allowedActions = $this->Acl->Aco->Permission->find('list', array(
                            'conditions' => array(
                                'Permission.aro_id' => $roleId,
                                'Permission.aco_id' => $thisControllerActionsIds,
                                'Permission._create' => 1,
                                'Permission._read' => 1,
                                'Permission._update' => 1,
                                'Permission._delete' => 1,
                            ),
                            'fields' => array(
                                'id',
                                'aco_id',
                            ),
                            'recursive' => '-1',
                        ));
                $allowedActionsIds = array_values($allowedActions);
            }

            $allow = array();
            if (isset($allowedActionsIds) &&
                    is_array($allowedActionsIds) &&
                    count($allowedActionsIds) > 0) {
                foreach ($allowedActionsIds AS $i => $aId) {
                    $allow[] = $thisControllerActions[$aId];
                }
            }

            $this->Auth->allowedActions = $allow;
        }
        //End permissions

        //gamba
        // if($this->_isAdminMode() && $this->Auth->user() && $this->Auth->user('grupo_id') != 1){
        //  $this->Session->setFlash(__('Acesso Negado'), 'default', array('class' => 'error_message'));
        //  $this->redirect(array('controller' => 'home', 'action' => 'index', 'admin' => false));
        // }
        
        // Allow all actions. CakePHP 2.0
        // $this->Auth->allow('*');
        // Allow all actions. CakePHP 2.1
        // $this->Auth->allow();
    }
}