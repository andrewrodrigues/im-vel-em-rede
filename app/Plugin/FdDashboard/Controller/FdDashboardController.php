<?php

    class FdDashboardController extends FdDashboardAppController
    {

        public $uses = array(
            'FdImoveis.ImovelVenda',
            'FdImoveis.Imobiliaria',
            'FdProjetos.Projeto'
        );

        public function fatorcms_index()
        {
            if (AuthComponent::user('master') || AuthComponent::user('constructor')) {
                $this->__relatorioPorImobiliaria(isset($this->request->query['imobiliaria']) ? $this->request->query['imobiliaria'] : null);
                $this->__relatorioPorCorretor(isset($this->request->query['corretor']) ? $this->request->query['corretor'] : null);
            } else {
                $this->redirect('/fatorcms/projetos');
            }
        }

        private function __relatorioPorImobiliaria($query = null)
        {
            $conditions = array();
            if (isset($query['data_inicio'])) {
                $conditions = array(
                    'Imobiliaria.created >=' => $this->convertDate($query['data_inicio']),
                    'Imobiliaria.created <=' => $this->convertDate($query['data_final'])
                );
            }
            if (!AuthComponent::user('master')) {
                $conditions += array(
                    'Imobiliaria.usuario_id' => AuthComponent::user('id')
                );
            }
            $vendas = $this->Imobiliaria->find('all', array( 'conditions' => $conditions, 'order' => 'totalVendidos DESC'));
            $this->set('vendasImobiliarias', $vendas);
        }

        private function __relatorioPorCorretor($query = null)
        {
            App::import('Model', 'FdImoveis.ImovelVenda');
            $this->ImovelVenda = new ImovelVenda();
            $conditions = array();
            if (isset($query['data_inicio'])) {
                $conditions = array(
                    'ImovelVenda.created >=' => $this->convertDate($query['data_inicio']),
                    'ImovelVenda.created <=' => $this->convertDate($query['data_final'])
                );
            }
            if (!AuthComponent::user('master')) {
                $conditions += array(
                    'Imobiliaria.usuario_id' => AuthComponent::user('id')
                );
                $vendas = $this->ImovelVenda->find('all',
                    array(
                        'conditions' => array(
                                'ImovelVenda.construtora_id' => AuthComponent::user('id')
                            ) + $conditions,
                        'group'      => 'ImovelVenda.corretor_id',
                        'order'      => array(
                            'ImovelVenda__totalCorretorVenda DESC'
                        )
                    )
                );
            } else {
                $vendas = $this->ImovelVenda->find('all',
                    array(
                        'conditions' => $conditions,
                        'group'      => 'ImovelVenda.corretor_id',
                        'order'      => array(
                            'ImovelVenda__totalCorretorVenda DESC'
                        )
                    )
                );
            }
            $this->set('vendasCorretores', $vendas);
        }

        private function convertDate($dateBr)
        {
            $date = DateTime::createFromFormat('d/m/Y', $dateBr);
            return $date->format('Y-m-d H:i:s');
        }

    }
