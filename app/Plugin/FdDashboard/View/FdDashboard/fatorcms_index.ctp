<?php $this->Html->css('fatorcms/dataTables/datatables.min', array('block' => 'css')); ?>
<?php $this->Html->addCrumb('Projetos') ?>


    <div class="row">
        <h3 class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
            Relatórios de vendas por imobiliária
        </h3>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <?php echo $this->Form->create('', array('type' => 'GET', 'url' => '/')) ?>
            <section class="panel">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="">
                                    Data início
                                </label>
                                <input type="text" value="<?php echo date('d/m/Y', strtotime(date('Y-m-d'). ' - 3 days')) ?>" name="imobiliaria[data_inicio]" class="form-control datepicker" />
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="">
                                    Data Final
                                </label>
                                <input type="text" value="<?php echo date('d/m/Y') ?>" name="imobiliaria[data_final]" class="form-control datepicker" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success" type="submit">
                            Procurar por data
                        </button>
                        <a href="<?php echo $this->Html->Url('/') ?>"
                           class="btn btn-primary">
                            Limpar
                        </a>
                    </div>
                </div>
            </section>
            <?php echo $this->Form->end() ?>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <div class="panel-body">
                    <table class="table table-bordered dataTables-example">
                        <thead>
                            <tr>
                                <?php if (AuthComponent::user('master') == true) { ?>
                                    <td width="20%">Construtora</td>
                                <?php } ?>
                                <td>Imobiliária</td>
                                <td>Total vendido</td>
                                <td>Data Venda</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if (isset($vendasImobiliarias) && count($vendasImobiliarias)) {
                                    foreach($vendasImobiliarias as $row) {
                                        ?>
                                        <tr class="bg-green" style="color: #FFF;">
                                            <?php if (AuthComponent::user('master') == true) { ?>
                                                <td><?php echo isset($row['Usuario']['nome']) ? $row['Usuario']['nome'] : ''; ?></td>
                                            <?php } ?>
                                            <td><?php echo $row['Imobiliaria']['nome']; ?></td>
                                            <td><?php echo $row['Imobiliaria']['totalVendidos'] ?></td>
                                            <td>-</td>
                                        </tr>
                                        <?php if (isset($row['ImovelVenda']) && count($row['ImovelVenda'])) { ?>
                                            <?php foreach ($row['ImovelVenda'] as $item) { ?>
                                                <tr>
                                                    <?php if (AuthComponent::user('master') == true) { ?>
                                                        <td>-</td>
                                                    <?php } ?>
                                                    <td><?php echo $this->String->getImobiliaria($item['imobiliaria_id'], 'nome') ?></td>
                                                    <td>-</td>
                                                    <td><?php echo $item['created_date'] ?></td>
                                                </tr>
                                            <?php } ?>
                                        <?php } ?>
                                        <?php
                                    }
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </div>


    <div class="row">
        <h3 class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
            Relatórios de vendas por corretor
        </h3>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <?php echo $this->Form->create('', array('type' => 'GET', 'url' => '/')) ?>
            <section class="panel">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="">
                                    Data início
                                </label>
                                <input type="text" value="<?php echo date('d/m/Y', strtotime(date('Y-m-d'). ' - 3 days')) ?>" name="corretor[data_inicio]" class="form-control datepicker" />
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="">
                                    Data Final
                                </label>
                                <input type="text" value="<?php echo date('d/m/Y') ?>" name="corretor[data_final]" class="form-control datepicker" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success" type="submit">
                            Procurar por data
                        </button>
                        <a href="<?php echo $this->Html->Url('/') ?>"
                           class="btn btn-primary">
                            Limpar
                        </a>
                    </div>
                </div>
            </section>
            <?php echo $this->Form->end() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <div class="panel-body">
                    <table class="table table-bordered dataTables-example">
                        <thead>
                            <tr>
                                <?php if (AuthComponent::user('master') == true) { ?>
                                    <td width="20%">Construtora</td>
                                <?php } ?>
                                <td>Corretor</td>
                                <td>Imobiliária</td>
                                <td>Quantidade vendida</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if (isset($vendasCorretores) && count($vendasCorretores)) {
                                    foreach($vendasCorretores as $row) {
                                        ?>
                                        <tr>
                                            <?php if (AuthComponent::user('master') == true) { ?>
                                                <td><?php echo isset($row['Construtora']['nome']) ? $row['Construtora']['nome'] : ''; ?></td>
                                            <?php } ?>
                                            <td><?php echo $row['Corretor']['nome'] ?></td>
                                            <td><?php echo $row['Imobiliaria']['nome']; ?></td>
                                            <td><?php echo $row[0]['ImovelVenda__totalCorretorVenda'] ?></td>
                                        </tr>
                                        <?php
                                    }
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </div>
<?php echo $this->Html->script('fatorcms/dataTables/datatables.min', array('block' => 'script')); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/buttons.colVis.min.js', array('block' => 'script')) ?>
<?php echo $this->Html->script('fatorcms/imoveis/imovel', array('block' => 'script')); ?>

