<?php
    Router::connect("/fatorcms/projetos",
        array(
            'plugin'     => 'fd_projetos',
            'controller' => 'fd_projetos',
            'action'     => 'index',
            'prefix'     => 'fatorcms',
            'fatorcms'   => true
        )
    );

    Router::connect("/fatorcms/projetos/:action/*",
        array(
            'plugin'     => 'fd_projetos',
            'controller' => 'fd_projetos',
            'prefix'     => 'fatorcms',
            'fatorcms'   => true
        )
    );

    Router::connect("/fatorcms/historicos/*",
        array(
            'plugin'     => 'fd_projetos',
            'controller' => 'fd_historicos',
            'action'     => 'index',
            'prefix'     => 'fatorcms',
            'fatorcms'   => true
        )
    );

    Router::connect("/fatorcms/convites",
        array(
            'plugin'     => 'fd_projetos',
            'controller' => 'fd_convites',
            'action'     => 'index',
            'prefix'     => 'fatorcms',
            'fatorcms'   => true
        )
    );

    Router::connect("/fatorcms/convites/:action/*",
        array(
            'plugin'     => 'fd_projetos',
            'controller' => 'fd_convites',
            'prefix'     => 'fatorcms',
            'fatorcms'   => true
        )
    );