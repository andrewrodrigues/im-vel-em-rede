<?php

    class Projeto extends FdProjetosAppModel
    {

        public $validate = array(
            'nome' => array(
                'notempty' => array(
                    'rule'    => array('notempty'),
                    'message' => 'Campo de preenchimento obrigatório.',
                )
            )
        );

        public $belongsTo = array(
            'Usuario' => array(
                'className'  => 'Usuario',
                'foreignKey' => 'usuario_id',
                'fields'     => array(
                    'Usuario.nome'
                )
            )
        );

        public $hasMany = array(
            'Coluna' => array(
                'className'  => 'Coluna',
                'foreignKey' => 'projeto_id'
            ),
            'Imovel' => array(
                'className'  => 'Imoveis',
                'foreignKey' => 'projeto_id',
                'conditions' => array(
                    'Imovel.deleted IS NULL'
                )
            )
        );

        public function getTotalProjetos($usuario_id = null)
        {
            return $this->find('count',
                array(
                    'conditions' => array(
                        'Projeto.usuario_id' => !is_null($usuario_id) ? $usuario_id : AuthComponent::user('id')
                    )
                )
            );
        }

        public function dados($projeto_id)
        {
            return $this->find('first',
                array(
                    'recursive'  => -1,
                    'conditions' => array(
                        'Projeto.id' => $projeto_id
                    )
                )
            );
        }

        public function destroy($id)
        {
            $data = parent::find('first', array('conditions' => array('Projeto.id' => $id)));
            if (!$data)
                return false;
            else {
                $update['Projeto']['id'] = $id;
                $update['Projeto']['deleted'] = date('Y-m-d H:i:s');
                if (parent::save($update))
                    return true;
                else {
                    return false;
                }
            }
        }

    }