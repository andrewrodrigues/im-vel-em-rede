<?php

    class ProjetoUsuario extends FdProjetosAppModel
    {

        public $useTable = 'projeto_usuarios';

        public function getProjetos($usuario_id = null)
        {
            return $this->find('list',
                array(
                    'recursive'  => -1,
                    'fields'     => array('ProjetoUsuario.projeto_id'),
                    'conditions' => array(
                        'ProjetoUsuario.usuario_id' => $usuario_id
                    )
                )
            );
        }

        public function usuarioProjeto($projeto_id, $usuario_id)
        {
            $exists = $this->find('first', array('conditions' => array('ProjetoUsuario.projeto_id' => $projeto_id, 'ProjetoUsuario.usuario_id' => $usuario_id)));
            if (!$exists) {
                $this->save(
                    array(
                        'ProjetoUsuario' => array(
                            'projeto_id' => $projeto_id,
                            'usuario_id' => $usuario_id
                        )
                    )
                );
            }
        }

    }