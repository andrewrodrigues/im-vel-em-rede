<?php

    class Historico extends FdProjetosAppModel
    {

        public $belongsTo = array(
            'Projeto' => array(
                'className'  => 'Projeto',
                'foreignKey' => 'projeto_id'
            )
        );

        public function customSave($projeto_id, $texto, $imovel_id = null)
        {
            $this->save(array(
                'Historico' => array(
                    'projeto_id' => $projeto_id,
                    'texto'      => $texto,
                    'imovel_id'  => $imovel_id
                )
            ));
        }

    }