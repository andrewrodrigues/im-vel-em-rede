<?php

    class Convite extends FdProjetosAppModel
    {

        public $belongsTo = array(
            'Projeto' => array(
                'className'  => 'Projeto',
                'foreignKey' => 'projeto_id'
            ),
            'Usuario' => array(
                'className'  => 'Usuario',
                'foreignKey' => 'usuario_id'
            )
        );

        public function getTotalConvitesEnviados()
        {
            $query = $this->find('count',
                array(
                    'conditions' => array(
                        'Convite.usuario_id' => AuthComponent::user('id'),
                        'Convite.deleted'    => null
                    ),
                    'group' => array('Convite.email')
                )
            );
            return $query;
        }

    }