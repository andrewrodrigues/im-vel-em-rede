<?php

    class FdHistoricosController extends FdProjetosAppController
    {

        public $uses = array('FdProjetos.Historico');

        public function fatorcms_index($projeto_id)
        {
            $data = $this->Historico->find('all', array('conditions' => array('Historico.projeto_id' => $projeto_id), 'order' => array('Historico.created DESC')));
            $this->set(compact('data', 'projeto_id'));
        }

    }