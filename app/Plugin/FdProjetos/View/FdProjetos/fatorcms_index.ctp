<?php $this->Html->css('fatorcms/dataTables/datatables.min', array('block' => 'css')); ?>
<?php $this->Html->addCrumb('Projetos') ?>

    <div class="row">
        <h3 class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
            <?php if (AuthComponent::user('master') || AuthComponent::user('constructor')) { ?>
                <a class="btn btn-success pull-right"
                   href="<?php echo $this->Html->Url('/fatorcms/projetos/add'); ?>">
                Cadastrar Projeto
            </a>
            <?php } ?>
            Projetos
        </h3>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <div class="panel-body">
                    <table class="display responsive no-wrap table table-striped table-bordered table-hover dataTables-example" width="100%">
                        <thead>
                            <tr>
                                <?php if (AuthComponent::user('master') == true) { ?>
                                    <td width="20%">Cliente</td>
                                <?php } ?>
                                <td title="Nome do projeto" width="30%">Nome</td>
                                <td width="7%"
                                    title="Quantidade de imóveis cadastrado no projeto">Quantidade</td>
                                <td width="7%">Disponível</td>
                                <td width="15%"
                                    title="Data em que o projeto foi criado">Data de Criação</td>
                                <td title="Opções disponíveis">Opções</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if (isset($data) && count($data)) {
                                    foreach ($data as $row) {
                                        ?>
                                        <tr>
                                            <td><?php echo $row['Projeto']['nome']; ?></td>
                                            <?php if (AuthComponent::user('master') == true) { ?>
                                                <td><?php echo isset($row['Usuario']['nome']) ? $row['Usuario']['nome'] : ''; ?></td>
                                            <?php } ?>
                                            <td align="center"><?php echo isset($row['Imovel']) ? count($row['Imovel']) : 0 ?></td>
                                            <td align="center">
                                                <?php
                                                    if (isset($row['Imovel'])) {
                                                        echo $this->String->totalDisponivel($row['Projeto']['id']);
                                                    }
                                                ?>
                                            </td>
                                            <td><?php echo $row['Projeto']['created'] ?></td>
                                            <td>
                                                <a href="<?php echo $this->Html->Url('/fatorcms/imoveis/index/' . $row['Projeto']['id']) ?>"
                                                   class="btn btn-xs btn-black"
                                                   title="Ver imóveis">
                                                    <i class="glyphicon glyphicon-home"></i>
                                                </a>
                                                <?php if (AuthComponent::user('master') || AuthComponent::user('constructor')) { ?>
                                                    <a class="btn btn-black btn-xs" href="<?php echo $this->Html->Url('/fatorcms/projetos/relatorio/' . $row['Projeto']['id']) ?>" title="Relatório de vendas">
                                                        <i class="fa fa-print"></i>
                                                    </a>
                                                    <?php if ($limit == true) { ?>
                                                        <a href="#"
                                                           data-id="<?php echo $row['Projeto']['id']; ?>"
                                                           data-toggle="modal"
                                                           data-loading="false"
                                                           data-target="#convite"
                                                           class="btn btn-xs btn-success"
                                                           title="Convidar um corretor para este projeto">
                                                            <i class="fa fa-plus"></i>
                                                        </a>
                                                    <?php } else { ?>
                                                        <a href=""
                                                           data-loading="false"
                                                           title="Sem limite para convites, contate o administrador"
                                                           class="btn btn-xs btn-danger no-limit">
                                                            <i class="fa fa-exclamation-triangle"></i>
                                                        </a>
                                                    <?php } ?>
                                                    <a href="<?php echo $this->Html->Url('/fatorcms/historicos/' . $row['Projeto']['id']); ?>"
                                                       title="Histórico do projeto [<?php echo $row['Projeto']['nome'] ?>]"
                                                       class="btn btn-xs btn-black">
                                                        <i class="fa fa-list"></i>
                                                    </a>
                                                    <a href="<?php echo $this->Html->Url('/fatorcms/projetos/edit/' . $row['Projeto']['id']); ?>"
                                                       class="btn btn-xs btn-primary"
                                                       title="Editar projeto - <?php echo $row['Projeto']['nome']; ?>">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                    <a href="<?php echo $this->Html->Url('/fatorcms/projetos/deletar/' . $row['Projeto']['id']); ?>"
                                                       class="btn btn-xs btn-danger deletar"
                                                       title="Deletar o projeto [<?php echo $row['Projeto']['nome']; ?>]">
                                                        <i class="fa fa-trash-o"></i>
                                                    </a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </div>

<?php if ($limit) { ?>
    <div id="convite"
         class="modal fade"
         role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button"
                    class="close"
                    data-dismiss="modal">&times;</button>
              <h4 class="modal-title">VINCULAR CORRETOR AO PROJETO</h4>
          </div>
            <?php echo $this->Form->create('FdProjetos.Convite', array('url' => '/fatorcms/convites/fastStore/', 'id' => 'formConvite')); ?>
            <div class="modal-body">
              <div class="row">
                  <?php if (AuthComponent::user('master')) { ?>
                      <div class="col-lg-12">
                          <div class="form-group">
                               <label for="cliente">
                                    Selecione um cliente (opcional)
                               </label>
                              <?php echo $this->Form->select('usuario_id', $clientes, array('class' => 'form-control', 'id' => 'convite-cliente')) ?>
                          </div>
                          <div class="form-group" style="display: none;">
                              <label for="projetos">
                                  Projetos
                              </label>
                              <?php echo $this->Form->select('projeto_id', array(), array('class' => 'form-control', 'id' => 'convite-projeto')) ?>
                          </div>
                      </div>
                      <script type="text/javascript">
                          $(document).ready(function () {
                              $('#convite-cliente').change(function () {
                                  var $this = $(this);
                                  var $convite = $('#convite-projeto');
                                  $convite.attr('required', 'required');
                                  if ($this.val() != '') {
                                      $.ajax({
                                          url: '<?php echo $this->Html->Url('/fatorcms/projetos/projetos_ajax'); ?>',
                                          data: {usuario_id: $this.val()},
                                          dataType: 'json',
                                          type: 'POST',
                                          success: function (res) {
                                              if (res.error == false) {
                                                  $convite.html($('<option>', { value: '' }).attr('selected', 'selected').text('Selecione um projeto'));
                                                  $.each(res.data, function(i, obj) {
                                                      $convite.append($('<option>', { value: i }).text(obj));
                                                  });
                                                  $convite.parent('.form-group').show();
                                              } else {
                                                  $convite.parent('.form-group').show();
                                                  $convite.html($('<option>', { value: '' }).attr('selected', 'selected').text('Nenhum projeto encontrado para a construtora selecionada.'));
                                              }
                                          }
                                      });
                                  } else {
                                      $convite.parent('.form-group').hide();
                                      $convite.removeAttr('required');
                                  }
                              });
                          });
                      </script>
                  <?php } ?>
                  <div class="col-lg-12">
                      <div class="form-group">
                           <label for="cliente">
                                Selecione o corretor <strong class="text-danger">(*)</strong>
                           </label>
                          <?php echo $this->Form->select('corretor_id', $corretores, array('class' => 'form-control')) ?>
                      </div>

                      <p class="text-info">
                          Para cadastrar um novo corretor, <a class="btn btn-xs btn-danger" href="<?php echo $this->Html->Url('/fatorcms/convites/add') ?>">clique aqui</a>.
                      </p>
                  </div>

              </div>
          </div>
          <div class="modal-footer">
              <button type="submit"
                      class="btn btn-success">Salvar vínculo</button>
              <button type="button"
                      class="btn btn-default"
                      data-dismiss="modal">Fechar</button>
          </div>
            <?php echo $this->Form->end(); ?>
        </div>

      </div>
    </div>
<?php } ?>

<?php echo $this->Html->script('fatorcms/dataTables/datatables.min', array('block' => 'script')); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/buttons.colVis.min.js', array('block' => 'script')) ?>
<?php echo $this->Html->script('fatorcms/imoveis/imovel', array('block' => 'script')); ?>