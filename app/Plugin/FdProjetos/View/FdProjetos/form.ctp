<div class="row">
    <?php if (in_array(AuthComponent::user('grupo_id'), array(1, 2))) { ?>
    <div class="col-lg-4">
        <div class="form-group">
            <label for="">
                Cliente<strong class="text-danger">*</strong>
            </label>
            <?php
                echo $this->Form->select('usuario_id',
                    $clientes,
                    array(
                        'default' => !isset($this->data['Projeto']['id']) ? 0 : $this->data['Projeto']['usuario_id'],
                        'class' => 'form-control'
                    )
                );
            ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <?php } ?>
    <div class="col-lg-4">
        <div class="form-group">
            <?php
                echo $this->Form->input('nome', array(
                        'class' => 'form-control',
                        'maxlength' => 50,
                        'placeholder' => 'Máximo 50 caractéres'
                    )
                );
            ?>
        </div>
    </div>
</div>

<button type="submit" class="btn btn-success">
    Salvar Projeto
</button>