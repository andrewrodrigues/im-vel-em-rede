<?php $this->Html->addCrumb('Projetos', array('action' => 'index')); ?>
<?php $this->Html->addCrumb('Editar Projeto'); ?>

<div class="row">
    <h3 class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
        <a href="<?php echo $this->Html->Url('/fatorcms/projetos'); ?>"
           class="btn btn-info pull-right">Voltar</a>
        Adicionar Projeto
    </h3>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <?php echo $this->Form->create('FdProjetos.Projeto', array('url' => '/fatorcms/projetos/update/'.$this->data['Projeto']['id'])) ?>
                <?php include 'form.ctp' ?>
                <?php echo $this->Form->end() ?>
            </div>
        </section>
    </div>
</div>