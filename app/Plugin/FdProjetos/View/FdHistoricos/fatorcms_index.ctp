<?php $this->Html->css('fatorcms/dataTables/datatables.min', array('block' => 'css')); ?>
<?php $this->Html->addCrumb('Projetos','/fatorcms/projetos')?>
<?php $this->Html->addCrumb('Histórico') ?>

    <div class="row">
        <h3 class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
            <a href="<?php echo $this->Html->Url('/fatorcms/projetos'); ?>"
               class="btn btn-info pull-right">Voltar</a>
            Históricos
        </h3>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <?php echo $this->Form->create('', array('type' => 'GET', 'url' => '/')) ?>
            <section class="panel">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="">
                                    Data início
                                </label>
                                <input type="text" value="<?php echo date('d/m/Y', strtotime(date('Y-m-d'). ' - 3 days')) ?>" name="imobiliaria[data_inicio]" class="form-control datepicker" />
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="">
                                    Data Final
                                </label>
                                <input type="text" value="<?php echo date('d/m/Y') ?>" name="imobiliaria[data_final]" class="form-control datepicker" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success" type="submit">
                            Procurar por data
                        </button>
                        <a href="<?php echo $this->Html->Url('/') ?>"
                           class="btn btn-primary">
                            Limpar
                        </a>
                    </div>
                </div>
            </section>
            <?php echo $this->Form->end() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover dataTables-example">
                        <thead>
                            <tr>
                                <td>Projeto</td>
                                <td>Mensagem</td>
                                <td width="15%">Data da Ação</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if (isset($data) && count($data)) {
                                    foreach($data as $row) {
                                        ?>
                                        <tr>
                                            <td><?php echo $row['Projeto']['nome']; ?></td>
                                            <td><?php echo $row['Historico']['texto']; ?></td>
                                            <td><?php echo $row['Historico']['created']; ?></td>
                                        </tr>
                                        <?php
                                    }
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </div>

<?php echo $this->Html->script('fatorcms/dataTables/datatables.min', array('block' => 'script')); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/buttons.colVis.min.js', array('block' => 'script')) ?>
<?php echo $this->Html->script('fatorcms/imoveis/imovel', array('block' => 'script')); ?>