<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
            <label for="nome">
                Nome Completo <strong class="text-danger">(*)</strong>
            </label>
            <?php
                echo $this->Form->input('nome', array(
                        'class' => 'form-control',
                        'label' => false,
                        'value' => $exists['Convite']['nome']
                    )
                );
            ?>
        </div>
        <div class="form-group">
            <label for="nome">
                Telefone <strong class="text-danger">(*)</strong>
            </label>
            <?php
                echo $this->Form->input('telefone', array(
                        'class'     => 'form-control',
                        'maxlength' => 50,
                        'label'     => false,
                        'value'     => $exists['Convite']['telefone']
                    )
                );
            ?>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <label for="nome">
                Senha <strong class="text-danger">(*)</strong>
            </label>
            <?php
                echo $this->Form->password('senha_nova', array(
                        'class'     => 'form-control',
                        'maxlength' => 50,
                        'label'     => false
                    )
                );
            ?>
        </div>
        <div class="form-group">
            <label for="nome">
                Confirma a Senha <strong class="text-danger">(*)</strong>
            </label>
            <?php
                echo $this->Form->password('confirma', array(
                        'class'     => 'form-control',
                        'maxlength' => 50,
                        'label'     => false
                    )
                );
            ?>
        </div>
    </div>
</div>


<button type="submit"
        onclick="return submitCorretor();"
        id="submitCorretor"
        class="btn btn-success pull-right">
    Cadastrar
</button>
