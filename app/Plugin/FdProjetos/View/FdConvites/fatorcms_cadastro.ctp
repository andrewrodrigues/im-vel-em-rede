<?php $this->Html->addCrumb('Projetos', array('action' => 'index')) ?>
<?php $this->Html->addCrumb('Adicionar Projeto') ?>

<div class="row"
     style="margin-top: 20%;">
    <div class="col-lg-12">
        <section class="panel"
                 style="margin-bottom: 0; -webkit-border-radius:0 ;-moz-border-radius:0 ;border-radius: 0;">
            <div class="panel-body">
                <div class="text-center">
                    <?php echo $this->Html->image('/img/fatorcms/mini-logo.png', array('alt' => 'Imóveis em Rede', 'class' => 'responsive', 'title' => 'Imóveis em Rede')); ?>
                </div>
            </div>
        </section>
        <h4 class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
            Para efetuar seu cadastro, por favor, preencha os campos abaixo.
        </h4>
        <section class="panel" style="margin-top: -5px">
            <div class="panel-body">
                <?php echo $this->Form->create('FdUsuarios.Usuario', array('url' => '/fatorcms/convites/storeCadastro')) ?>
                <?php include 'form.ctp' ?>
                <small>Todos os campos seguidos de <strong class="text-danger">(*)</strong> são obrigatórios.</small>
                <?php echo $this->Form->end() ?>
            </div>
        </section>
    </div>
</div>