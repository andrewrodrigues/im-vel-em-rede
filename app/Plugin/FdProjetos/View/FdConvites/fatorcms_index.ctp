<?php $this->Html->css('fatorcms/dataTables/datatables.min', array('block' => 'css')); ?>
<?php $this->Html->addCrumb('Projetos') ?>

    <div class="row">
        <h3 class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
            <a href="<?php echo $this->Html->Url('/fatorcms/convites/add/'); ?>"
                class="btn btn-primary pull-right">Cadastrar Corretor</a>
            <a href="<?php echo $this->Html->Url('/fatorcms/projetos'); ?>"
               class="btn btn-info pull-right"
               style="margin-right: 5px;">Voltar</a>
            Gestão de Convites
        </h3>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover dataTables-example">
                        <thead>
                            <tr>
                                <?php if (AuthComponent::user('master') == true) { ?>
                                    <td width="20%">Cliente</td>
                                <?php } ?>
                                <td>Projeto</td>
                                <td>Nome</td>
                                <td>E-mail</td>
                                <td>Telefone</td>
                                <td>Status</td>
                                <td>Data do convite</td>
                                <td width="3%">Opções</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if (isset($data) && count($data)) {
                                    foreach($data as $row) {
                                        ?>
                                        <tr>
                                            <?php if (AuthComponent::user('master') == true) { ?>
                                                <td><?php echo isset($row['Usuario']['nome']) ? $row['Usuario']['nome'] : ''; ?></td>
                                            <?php } ?>
                                            <td><?php echo $row['Projeto']['nome']; ?></td>
                                            <td><?php echo $row['Convite']['nome']; ?></td>
                                            <td><?php echo $row['Convite']['email']; ?></td>
                                            <td><?php echo $row['Convite']['telefone']; ?></td>
                                            <td><?php echo $row['Convite']['aceito'] ? 'Convite aceito' : 'Ainda não aceitou o convite'; ?></td>
                                            <td><?php echo $row['Convite']['created'] ?></td>
                                            <td>
                                                <a href="<?php echo $this->Html->Url('/fatorcms/convites/deletar/' . $row['Convite']['id']); ?>"
                                                   class="btn btn-xs btn-danger">
                                                    <i class="fa fa-trash-o"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </div>

<?php echo $this->Html->script('fatorcms/dataTables/datatables.min', array('block' => 'script')); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/buttons.colVis.min.js', array('block' => 'script')) ?>
<?php echo $this->Html->script('fatorcms/imoveis/imovel', array('block' => 'script')); ?>