<div class="row">
    <?php if (AuthComponent::user('master')) { ?>
      <div class="col-lg-12">
          <div class="form-group">
               <label for="cliente">
                    Selecione um cliente (opcional)
               </label>
              <?php echo $this->Form->select('usuario_id', $clientes, array('class' => 'form-control', 'id' => 'convite-cliente')) ?>
          </div>
          <div class="form-group" style="display: none;">
              <label for="projetos">
                  Projetos
              </label>
              <?php echo $this->Form->select('projeto_id', array(), array('class' => 'form-control', 'id' => 'convite-projeto')) ?>
          </div>
      </div>
      <script type="text/javascript">
          $(document).ready(function () {
              $('#convite-cliente').change(function () {
                  var $this = $(this);
                  var $convite = $('#convite-projeto');
                  $convite.attr('required', 'required');
                  if ($this.val() != '') {
                      $.ajax({
                          url: '<?php echo $this->Html->Url('/fatorcms/projetos/projetos_ajax'); ?>',
                          data: {usuario_id: $this.val()},
                          dataType: 'json',
                          type: 'POST',
                          success: function (res) {
                              if (res.error == false) {
                                  $convite.html($('<option>', { value: '' }).attr('selected', 'selected').text('Selecione um projeto'));
                                  $.each(res.data, function(i, obj) {
                                      $convite.append($('<option>', { value: i }).text(obj));
                                  });
                                  $convite.parent('.form-group').show();
                              } else {
                                  $convite.parent('.form-group').show();
                                  $convite.html($('<option>', { value: '' }).attr('selected', 'selected').text('Nenhum projeto encontrado para a construtora selecionada.'));
                              }
                          }
                      });
                  } else {
                      $convite.parent('.form-group').hide();
                      $convite.removeAttr('required');
                  }
              });
          });
      </script>
    <?php } ?>

    <?php if (AuthComponent::user("constructor") == 1) { ?>
    <div class="col-lg-12">
      <div class="form-group">
          <label for="projetos">
              Projetos<strong class="text-danger">*</strong>
          </label>
        <?php echo $this->Form->select('projeto_id', $projetos, array('class' => 'form-control', 'id' => 'convite-projeto')) ?>
      </div>
    </div>
    <?php } ?>
    <div class="col-lg-6">
      <div class="form-group">
          <label for="nome">
              Nome<strong class="text-danger">*</strong>
          </label>
          <?php echo $this->Form->text('nome', array('class' => 'form-control')) ?>
      </div>
      <div class="form-group">
          <label for="nome">
              Telefone<strong class="text-danger">*</strong>
          </label>
          <?php echo $this->Form->text('telefone', array('class' => 'form-control')) ?>
      </div>
    </div>
    <div class="col-lg-6">
      <div class="form-group">
          <label for="nome">
              E-mail<strong class="text-danger">*</strong>
          </label>
          <?php echo $this->Form->text('email', array('class' => 'form-control')) ?>
      </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="nome">
                        Senha <strong class="text-danger">(*)</strong>
                    </label>
                    <?php
                        echo $this->Form->password('senha_nova', array(
                                'class'     => 'form-control',
                                'maxlength' => 50,
                                'label'     => false
                            )
                        );
                    ?>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="nome">
                        Confirma a Senha <strong class="text-danger">(*)</strong>
                    </label>
                    <?php
                        echo $this->Form->password('confirma', array(
                                'class'     => 'form-control',
                                'maxlength' => 50,
                                'label'     => false
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <button type="submit" onclick="return submitCorretor()" class="btn btn-success">
            Cadastrar Corretor
        </button>
    </div>
</div>

<?php echo $this->Html->script('fatorcms/imoveis/convite', array('block' => 'script')); ?>



<script type="text/javascript">
    function submitCorretor () {
        var $senha = $('#ConviteSenhaNova').val();
        var $confirma = $('#ConviteConfirma').val();
        if ($senha != $confirma || ($senha == '' && $confirma == '')) {
            swal('Imóveis em rede', 'As senhas não coincidem.', 'error');
            return false;
        } else {
            return true;
        }
    }
</script>

