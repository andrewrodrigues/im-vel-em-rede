<?php $this->Html->addCrumb('Convites', array('action' => 'index')) ?>
<?php $this->Html->addCrumb('Cadastrar Corretor') ?>

<div class="row">
    <h3 class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
        <a href="<?php echo $this->Html->Url('/fatorcms/convites'); ?>"
           class="btn btn-info pull-right">Voltar</a>
        Cadastrar Corretor
    </h3>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <?php echo $this->Form->create('FdProjetos.Convite', array('url' => '/fatorcms/convites/store/', 'id' => 'formConvite')); ?>
                <?php include 'form-convite.ctp' ?>
                <?php echo $this->Form->end() ?>
            </div>
        </section>
    </div>
</div>