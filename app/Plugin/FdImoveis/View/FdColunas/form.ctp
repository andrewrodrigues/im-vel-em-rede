<div class="row">
    <div class="col-lg-3">
        <div class="form-group">
            <label for="nome">
                Ordem da coluna<strong class="text-danger">*</strong>
            </label>
            <?php
                echo $this->Form->text('ordem',
                    array(
                        'class'    => 'form-control',
                        'required' => 'required'
                    )
                );
            ?>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group">
            <label for="nome">
                Coluna Privada<strong class="text-danger">*</strong>
            </label>
            <div class="clearfix"></div>
            <?php
                echo $this->Form->checkbox('privado',
                    array(
                        'value'    => '1'
                    )
                );
            ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-lg-6">
        <div class="form-group">
            <label for="nome">
                Nome da coluna<strong class="text-danger">*</strong>
            </label>
            <?php
                echo $this->Form->text('nome',
                    array(
                        'class'    => 'form-control',
                        'required' => 'required'
                    )
                );
            ?>
        </div>
    </div>
</div>

<button type="submit" class="btn btn-success">
    Salvar Registros
</button>