<?php $this->Html->css('fatorcms/dataTables/datatables.min', array('block' => 'css')); ?>
<?php $this->Html->addCrumb('Imóveis', '/fatorcms/imoveis/index/' . $id) ?>
<?php $this->Html->addCrumb('Listar Colunas') ?>

    <div class="row">
        <h3 class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
            <a href="<?php echo $this->Html->Url('/fatorcms/imoveis/index/' . $id); ?>"
               class="btn btn-info pull-right">Voltar</a>
            Colunas
        </h3>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover dataTables-example">
                        <thead>
                            <tr>
                                <td>Nome</td>
                                <td width="5%">Ordem</td>
                                <td width="15%">Data da Ação</td>
                                <td width="5%">Opções</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if (isset($data) && count($data)) {
                                    foreach ($data as $row) {
                                        ?>
                                        <tr>
                                            <td><?php echo $row['Coluna']['nome']; ?></td>
                                            <td align="center"><?php echo $row['Coluna']['ordem']; ?></td>
                                            <td><?php echo $row['Coluna']['created']; ?></td>
                                            <td>
                                                <a href="<?php echo $this->Html->Url('/fatorcms/colunas/edit/' . $id . '/' . $row['Coluna']['id']); ?>"
                                                   class="btn btn-xs btn-primary"
                                                   title="Editar Coluna - <?php echo $row['Coluna']['nome']; ?>">
                                                <i class="fa fa-pencil"></i>
                                                </a>
                                                <a href="<?php echo $this->Html->Url('/fatorcms/colunas/deletar/' . $id . '/' . $row['Coluna']['id']); ?>"
                                                   class="btn btn-xs btn-danger">
                                                    <i class="fa fa-trash-o"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </div>

<?php echo $this->Html->script('fatorcms/dataTables/datatables.min', array('block' => 'script')); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/buttons.colVis.min.js', array('block' => 'script')) ?>
<?php echo $this->Html->script('fatorcms/imoveis/imovel', array('block' => 'script')); ?>