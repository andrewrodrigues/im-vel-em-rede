<?php $this->Html->addCrumb('Imóveis', '/fatorcms/imoveis/index/' . $projeto_id) ?>
<?php $this->Html->addCrumb('Adicionar Campo') ?>

<div class="row">
    <h3 class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
        <a href="<?php echo $this->Html->Url('/fatorcms/imoveis/index/' . $projeto_id); ?>"
           class="btn btn-info pull-right">Voltar</a>
        Adicionar Campos
    </h3>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <?php echo $this->Form->create('FdImoveis.Coluna', array('url' => '/fatorcms/colunas/store/' . $projeto_id)) ?>
                <?php include 'form.ctp' ?>
                <?php echo $this->Form->end() ?>
            </div>
        </section>
    </div>
</div>