<?php $this->Html->css('fatorcms/dataTables/datatables.min', array('block' => 'css')); ?>
<?php $this->Html->css('https://cdn.datatables.net/responsive/1.0.7/css/responsive.dataTables.min.css', array('block' => 'css')); ?>
<?php $this->Html->addCrumb('Projetos', array('action' => 'index', 'plugin' => 'projetos', 'controller' => '')) ?>
<?php $this->Html->addCrumb('Imóveis') ?>

<div class="row">
        <h3 class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
            <?php if (AuthComponent::user('master') || AuthComponent::user('constructor')) { ?>
                <a href="<?php echo $this->Html->Url('/fatorcms/colunas/index/' . $id); ?>"
                   class="btn btn-danger pull-right"
                   style="margin-left: 5px;">Listar/Editar Colunas</a>
                <a href="<?php echo $this->Html->Url('/fatorcms/colunas/add/' . $id); ?>"
                   class="btn btn-primary pull-right">Adicionar Coluna</a>
                <a href="<?php echo $this->Html->Url('/fatorcms/imoveis/add/' . $id); ?>"
                   class="btn btn-success pull-right">Adicionar Imóvel &nbsp;</a>
            <?php } ?>
            <a href="<?php echo $this->Html->Url('/fatorcms/projetos'); ?>"
               class="btn btn-info pull-right"
               style="margin-right: 5px;">Voltar</a>
            <?php echo $projeto['Projeto']['nome']; ?> - Imóveis
        </h3>
    </div>

<?php if (AuthComponent::user('master') || AuthComponent::user('constructor')) { ?>
    <?php echo $this->Form->create('', array('url' => '/fatorcms/imoveis/storeNotification/' . $projeto['Projeto']['id'])) ?>
    <div class="alert alert-info">
        <textarea name="data[Projeto][notificacao]"
                  rows="2"
                  id="redactor"
                  class="form-control"
                  placeholder="Digite uma notificação para todos os corretores"><?php echo !empty($projeto['Projeto']['notificacao']) ? $projeto['Projeto']['notificacao'] : '' ?></textarea>
        <br/>
        <button type="submit"
                class="btn btn-success">
            Salvar
        </button>
    </div>
    <?php echo $this->Form->end() ?>
<?php } else if (!empty($projeto['Projeto']['notificacao'])) { ?>
    <div class="alert alert-info">
        <?php echo $projeto['Projeto']['notificacao'] ?>
    </div>
<?php } ?>

<div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <div class="panel-body">
                    <table class="display responsive no-wrap table table-striped table-bordered table-hover dataTables-example"
                           width="100%">
                        <thead>
                            <tr>
                                <?php foreach ($colunas as $coluna) { ?>
                                    <?php if ($coluna['Coluna']['projeto_id'] == 0 || $coluna['Coluna']['privado'] == 0 || AuthComponent::user('master') || AuthComponent::user('constructor')) { ?>
                                        <td data-id="<?php echo $coluna['Coluna']['id']; ?>"
                                            data-alias="<?php echo $coluna['Coluna']['alias']; ?>">
                                        <?php echo $coluna['Coluna']['nome']; ?>
                                    </td>
                                    <?php } ?>
                                <?php } ?>
                                <td width="10%">Opções</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if (isset($data) && count($data)) {
                                    foreach ($data as $row) {
                                        $i = 0;
                                        $s = '';
                                        echo '<tr>';
                                        foreach ($colunas as $coluna) {
                                            if ($coluna['Coluna']['projeto_id'] == 0 || $coluna['Coluna']['privado'] == 0 || AuthComponent::user('master') || AuthComponent::user('constructor')) {
                                                $search = array_search($coluna['Coluna']['id'], array_column($row['ColunaImovelValor'], 'coluna_id'));
                                                if ($search !== false) {
                                                    if ($coluna['Coluna']['alias'] == 'status') {
                                                        $s = $search;
                                                        echo '<td>' . $status[$row['ColunaImovelValor'][$search]['valor']] . '</td>';
                                                    } else {
                                                        echo '<td>' . $row['ColunaImovelValor'][$search]['valor'] . '</td>';
                                                    }
                                                } else if ($i == 0) {
                                                    echo '<td>' . $row['Imovel']['nome'] . '</td>';
                                                } else {
                                                    echo '<td></td>';
                                                }
                                            }
                                            $i++;
                                        }
                                        ?>
                                        <td>
                                            <?php if ((AuthComponent::user('master') || AuthComponent::user('constructor')) && $row['ColunaImovelValor'][$s]['valor'] != 3) { ?>
                                                <a href="#"
                                                   data-loading="false"
                                                   data-target="#vendido"
                                                   data-toggle="modal"
                                                   title="Imóvel vendido"
                                                   data-id="<?php echo $this->Html->Url('/fatorcms/imoveis/venda/' . $projeto['Projeto']['usuario_id'] . '/' . $row['Imovel']['projeto_id'] . '/' . $row['Imovel']['id'] . '/' . $row['ColunaImovelValor'][$s]['id']); ?>"
                                                   class="btn btn-xs btn-primary">
                                                            <i class="fa fa-usd"></i>
                                                        </a>
                                            <?php } ?>
                                            <?php if ($row['ColunaImovelValor'][$s]['valor'] == 1) { ?>
                                                <a href="#"
                                                   data-target="#reservar"
                                                   data-toggle="modal"
                                                   data-id="<?php echo $this->Html->Url('/fatorcms/imoveis/reservar/' . $id . '/' . $row['Imovel']['id'] . '/' . $row['ColunaImovelValor'][$s]['id']); ?>"
                                                   data-loading="false"
                                                   title="Reservar Imóvel"
                                                   class="btn btn-xs btn-info">
                                                    <i class="fa fa-star"></i>
                                                </a>
                                            <?php } else if ($row['ColunaImovelValor'][$s]['valor'] == 2) { ?>
                                                <?php if ($reserva || AuthComponent::user('constructor') || AuthComponent::user('master')) { ?>
                                                    <a href="<?php echo $this->Html->Url('/fatorcms/imoveis/removeReserva/' . $id . '/' . $row['Imovel']['id'] . '/' . $row['ColunaImovelValor'][$s]['id']); ?>"
                                                       title="Remover reserva"
                                                       class="btn btn-xs btn-danger">
                                                        <i class="fa fa-times"></i>
                                                    </a>
                                                    <a href="<?php echo $this->Html->Url('/fatorcms/reservas/show/visualizar/' . $row['Imovel']['id']) ?>"
                                                       title="Visualizar dados da reserva"
                                                       class="btn btn-xs btn-black">
                                                        <i class="fa fa-eye"></i>
                                                    </a>
                                                <?php } ?>
                                            <?php } ?>
                                            <?php if (AuthComponent::user('master') || AuthComponent::user('constructor')) { ?>
                                                <a href="<?php echo $this->Html->Url('/fatorcms/imoveis/edit/' . $id . '/' . $row['Imovel']['id']); ?>"
                                                   class="btn btn-xs btn-primary"
                                                   title="Editar projeto - <?php echo $row['Imovel']['nome']; ?>">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <a href="<?php echo $this->Html->Url('/fatorcms/imoveis/deletar/' . $id . '/' . $row['Imovel']['id']); ?>"
                                                   class="btn btn-xs btn-danger deleter">
                                                    <i class="fa fa-trash-o"></i>
                                                </a>
                                            <?php } ?>
                                        </td>
                                        <?php
                                        echo '</tr>';
                                    }
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </div>





    <div id="vendido"
         class="modal fade"
         role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
             <?php echo $this->Form->create('FdImoveis.ImovelVenda', array('role' => 'form', 'class' => 'minimal', 'id' => 'formVendido')) ?>
            <div class="modal-header">
                        <button type="button"
                                class="close"
                                data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Vender Imóvel</h4>
                        <p class="text-info">
                            Obs: você precisa ter pelo menos um corretor e uma imobiliária cadastrados.
                        </p>
                  </div>
                  <div class="modal-body">
                        <?php echo $this->Form->hidden('imovel_id'); ?>
                      <?php echo $this->Form->hidden('construtora_id'); ?>
                      <div class="row">
                            <div class="col-md-12 col-lg-12">
                                <div class="form-group">
                                    <?php echo $this->Form->input('imobiliaria_id',
                                        array(
                                            'label'   => 'Imobiliária <strong class="text-danger">(*)</strong>',
                                            'options' => array('' => 'Selecione uma imobiliária') + $imobiliarias,
                                            'class'   => 'form-control',
                                            'required' => 'required'
                                        )
                                    ); ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 col-lg-12">
                                <div class="form-group">
                                    <?php echo $this->Form->input('corretor_id',
                                        array(
                                            'label'   => 'Corretor <strong class="text-danger">(*)</strong>',
                                            'options' => array('' => 'Selecione o corretor') + $corretores,
                                            'class'   => 'form-control',
                                            'required' => 'required'
                                        )
                                    ); ?>
                                </div>
                            </div>
                        </div>
                  </div>
                  <div class="modal-footer">
                        <button type="submit"
                                class="btn btn-success">Vender Imóvel</button>
                        <button type="button"
                                class="btn btn-default"
                                data-dismiss="modal">Fechar</button>
                  </div>
            <?php echo $this->Form->end() ?>
        </div>
      </div>
    </div>




















    <div id="reservar"
         class="modal fade"
         role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
             <?php echo $this->Form->create('FdImoveis.Reserva', array('role' => 'form', 'class' => 'minimal', 'id' => 'formReserva')) ?>
            <div class="modal-header">
                        <button type="button"
                                class="close"
                                data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Reservar Imóvel</h4>
                        <p>Digite os campos abaixo para fazer a reserva.</p>
                  </div>
                  <div class="modal-body">
                        <?php echo $this->Form->hidden('imovel_id'); ?>
                      <?php echo $this->Form->hidden('construtora_id'); ?>
                      <div class="row">
                            <div class="col-md-12 col-lg-12">
                                <div class="form-group">
                                    <?php echo $this->Form->input('nome',
                                        array(
                                            'label'    => 'Nome do cliente <strong class="text-danger">(*)</strong>',
                                            'class'    => 'form-control',
                                            'required' => 'required'
                                        )
                                    ); ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 col-lg-12">
                                <div class="form-group">
                                    <?php echo $this->Form->input('email',
                                        array(
                                            'label'    => 'E-mail para contato <strong class="text-danger">(*)</strong>',
                                            'class'    => 'form-control',
                                            'required' => 'required'
                                        )
                                    ); ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 col-lg-12">
                                <div class="form-group">
                                    <?php echo $this->Form->input('telefone',
                                        array(
                                            'label'    => 'Telefone para contato <strong class="text-danger">(*)</strong>',
                                            'class'    => 'form-control',
                                            'required' => 'required'
                                        )
                                    ); ?>
                                </div>
                            </div>
                        </div>
                  </div>
                  <div class="modal-footer">
                        <button type="submit"
                                class="btn btn-success">Reservar Imóvel</button>
                        <button type="button"
                                class="btn btn-default"
                                data-dismiss="modal">Fechar</button>
                  </div>
            <?php echo $this->Form->end() ?>
        </div>
      </div>
    </div>

<?php echo $this->Html->script('fatorcms/dataTables/datatables.min', array('block' => 'script')); ?>
<?php echo $this->Html->script('https://cdn.datatables.net/responsive/2.0.2/js/dataTables.responsive.min.js', array('block' => 'script')); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/buttons.colVis.min.js', array('block' => 'script')) ?>
<?php echo $this->Html->script('fatorcms/imoveis/imovel', array('block' => 'script')); ?>
