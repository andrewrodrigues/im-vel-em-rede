<div class="row">
    <?php $i = 0;
        $broke = round(count($colunas) / 2); ?>
    <div class="col-lg-6">

        <?php foreach ($colunas as $coluna) { ?>
            <?php if ($coluna['Coluna']['alias'] != 'status') { ?>
                <div class="form-group">
                    <?php
                        echo $this->Form->input($coluna['Coluna']['alias'], array(
                                'class' => 'form-control',
                                'label' => $coluna['Coluna']['nome']
                            )
                        );
                    ?>
                </div>
            <?php } else { ?>
                <div class="form-group">
                    <label for="status">
                        Status
                    </label>
                    <?php
                        echo $this->Form->select('status', $status, array(
                                'class'   => 'form-control',
                                'label'   => $coluna['Coluna']['nome'],
                                'default' => isset($this->data['Imovel']['status']) ? $this->data['Imovel']['status'] : 1
                            )
                        );
                    ?>
                </div>
            <?php } ?>
            <?php echo ($i / $broke == 0) ? '</div><div class="col-lg-6">' : ''; ?>
        <?php } ?>

    </div>
</div>




<div id="vendido" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Vender Imóvel</h4>
            <p>Digite os campos abaixo para fazer a venda.</p>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div class="form-group">
                        <?php echo $this->Form->input('imobiliaria_id',
                            array(
                                'label'    => 'Imobiliária <strong class="text-danger">(*)</strong>',
                                'options'  => array('' => 'Selecione uma imobiliária') + $imobiliarias,
                                'class'    => 'form-control'
                            )
                        ); ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div class="form-group">
                        <?php echo $this->Form->input('corretor_id',
                            array(
                                'label'    => 'Corretor <strong class="text-danger">(*)</strong>',
                                'options'  => array('' => 'Selecione o corretor') + $corretores,
                                'class'    => 'form-control'
                            )
                        ); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-success">Vender Imóvel</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        </div>
    </div>
  </div>
</div>


<button type="submit"
        class="btn btn-success">
    Salvar Imóvel
</button>

<?php echo $this->Html->script('fatorcms/imoveis/crud', array('block' => 'script')); ?>