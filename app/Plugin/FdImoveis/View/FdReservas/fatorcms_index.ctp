<?php $this->Html->css('fatorcms/dataTables/datatables.min', array('block' => 'css')); ?>
<?php $this->Html->addCrumb('Projetos', '/fatorcms/projetos') ?>
<?php $this->Html->addCrumb('Reservas') ?>

    <div class="row">
        <h3 class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
            <a href="<?php echo $this->Html->Url('/fatorcms/projetos'); ?>"
               class="btn btn-info pull-right">Voltar</a>
            Reservas
        </h3>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover dataTables-example">
                        <thead>
                            <tr>
                                <td>Nome</td>
                                <td>E-mail</td>
                                <td>Telefone</td>
                                <td width="15%">Data da Reserva</td>
                                <td width="5%">Opções</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if (isset($data) && count($data)) {
                                    foreach ($data as $row) {
                                        ?>
                                        <tr>
                                            <td><?php echo $row['Reserva']['nome']; ?></td>
                                            <td><?php echo $row['Reserva']['email']; ?></td>
                                            <td><?php echo $row['Reserva']['telefone']; ?></td>
                                            <td><?php echo $row['Reserva']['created']; ?></td>
                                            <td>
                                                <a href="<?php echo $this->Html->Url('/fatorcms/reservas/show/' . $row['Reserva']['id']); ?>"
                                                   title="Visualizar os dados da reserva"
                                                   class="btn btn-xs">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </div>

<?php echo $this->Html->script('fatorcms/dataTables/datatables.min', array('block' => 'script')); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/buttons.colVis.min.js', array('block' => 'script')) ?>
<?php echo $this->Html->script('fatorcms/imoveis/imovel', array('block' => 'script')); ?>