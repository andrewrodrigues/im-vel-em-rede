<?php $this->Html->css('fatorcms/dataTables/datatables.min', array('block' => 'css')); ?>
<?php $this->Html->addCrumb('Projetos', '/fatorcms/projetos') ?>
<?php $this->Html->addCrumb('Reservas') ?>

    <div class="row">
        <h3 class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
            <a href="<?php echo $this->Html->Url($referer); ?>"
               class="btn btn-info pull-right">Voltar</a>
            Reserva do imóvel <?php echo $data['Imovel']['nome']; ?> - Projeto [<?php echo $data['Projeto']['nome']; ?>]
        </h3>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <div class="panel-body">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="nome">
                                Nome do contato
                            </label>
                            <p>
                                <?php echo $data['Reserva']['nome']; ?>
                            </p>
                        </div>
                        <div class="form-group">
                            <label for="email">
                                E-mail do contato
                            </label>
                            <p>
                                <?php echo $data['Reserva']['email']; ?>
                            </p>
                        </div>
                        <div class="form-group">
                            <label for="nome">
                                Telefone do contato
                            </label>
                            <p>
                                <?php echo $data['Reserva']['telefone']; ?>
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="imovel">
                                Imóvel
                            </label>
                            <p>
                                <?php echo $data['Imovel']['nome']; ?>
                            </p>
                        </div>
                        <div class="form-group">
                            <label for="projeto">
                                Projeto
                            </label>
                            <p>
                                <?php echo $data['Projeto']['nome']; ?>
                            </p>
                        </div>
                    </div>
                    <div class="divider clearfix" style="border-bottom: 1px solid #999; margin-bottom: 10px;"></div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="imovel">
                                Corretor
                            </label>
                            <p>
                                <?php echo $data['Corretor']['nome']; ?>
                            </p>
                        </div>
                        <div class="form-group">
                            <label for="projeto">
                                Corretor Telefone
                            </label>
                            <p>
                                <?php echo $data['Corretor']['telefone']; ?>
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="imovel">
                                Corretor E-mail
                            </label>
                            <p>
                                <?php echo $data['Corretor']['email']; ?>
                            </p>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

<?php echo $this->Html->script('fatorcms/dataTables/datatables.min', array('block' => 'script')); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/buttons.colVis.min.js', array('block' => 'script')) ?>
<?php echo $this->Html->script('fatorcms/imoveis/imovel', array('block' => 'script')); ?>