<?php $this->Html->css('fatorcms/dataTables/datatables.min', array('block' => 'css')); ?>
<?php $this->Html->addCrumb('Projetos', '/fatorcms/projetos') ?>
<?php $this->Html->addCrumb('Imobiliarias') ?>

    <div class="row">
        <h3 class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
            <a href="<?php echo $this->Html->Url('/fatorcms/imobiliarias/add'); ?>"
               class="btn btn-success pull-right">Cadastrar Imobiliária &nbsp;</a>
            <a href="<?php echo $this->Html->Url('/fatorcms/projetos'); ?>"
               class="btn btn-info pull-right">Voltar</a>
            Imobiliárias
        </h3>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover dataTables-example">
                        <thead>
                            <tr>
                                <?php if (AuthComponent::user('master')) { ?>
                                    <td>Construtora</td>
                                <?php } ?>
                                <td>Nome</td>
                                <td width="5%">Opções</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if (isset($data) && count($data)) {
                                    foreach ($data as $row) {
                                        ?>
                                        <tr>
                                            <?php if (AuthComponent::user('master')) { ?>
                                                <td><?php echo $row['Usuario']['nome'] ?></td>
                                            <?php } ?>
                                            <td><?php echo $row['Imobiliaria']['nome']; ?></td>

                                            <td>
                                                <?php if (AuthComponent::user('master') || AuthComponent::user('id') == $row['Imobiliaria']['usuario_id']) { ?>
                                                    <a href="<?php echo $this->Html->Url('/fatorcms/imobiliarias/edit/' . $row['Imobiliaria']['id']); ?>"
                                                       class="btn btn-xs btn-primary"
                                                       title="Editar Imobiliaria - <?php echo $row['Imobiliaria']['nome']; ?>">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                <?php } ?>
                                                <?php if (AuthComponent::user('master')) { ?>
                                                    <a href="<?php echo $this->Html->Url('/fatorcms/imobiliarias/deletar/' . $row['Imobiliaria']['id']); ?>"
                                                       class="btn btn-xs btn-danger">
                                                        <i class="fa fa-trash-o"></i>
                                                    </a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </div>

<?php echo $this->Html->script('fatorcms/dataTables/datatables.min', array('block' => 'script')); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/buttons.colVis.min.js', array('block' => 'script')) ?>