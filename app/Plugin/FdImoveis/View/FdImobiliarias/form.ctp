<div class="row">
    <div class="col-lg-3">
        <div class="form-group">
            <label for="nome">
                Nome<strong class="text-danger">*</strong>
            </label>
            <?php
                echo $this->Form->text('nome',
                    array(
                        'class'    => 'form-control',
                        'required' => 'required'
                    )
                );
            ?>
        </div>
    </div>
</div>

<button type="submit" class="btn btn-success">
    Salvar Registro
</button>