<?php
    Router::connect("/fatorcms/imoveis",
        array(
            'plugin'     => 'fd_imoveis',
            'controller' => 'fd_imoveis',
            'action'     => 'index',
            'prefix'     => 'fatorcms',
            'fatorcms'   => true
        )
    );

    Router::connect("/fatorcms/imoveis/:action/*",
        array(
            'plugin'     => 'fd_imoveis',
            'controller' => 'fd_imoveis',
            'prefix'     => 'fatorcms',
            'fatorcms'   => true
        )
    );

    Router::connect('/fatorcms/imobiliarias',
        array(
            'plugin'     => 'fd_imoveis',
            'controller' => 'fd_imobiliarias',
            'prefix'     => 'fatorcms',
            'action'     => 'index',
            'fatorcms'   => true
        )
    );

    Router::connect('/fatorcms/imobiliarias/:action/*',
        array(
            'plugin'     => 'fd_imoveis',
            'controller' => 'fd_imobiliarias',
            'prefix'     => 'fatorcms',
            'fatorcms'   => true
        )
    );

    Router::connect("/fatorcms/colunas/:action/*",
        array(
            'plugin'     => 'fd_imoveis',
            'controller' => 'fd_colunas',
            'prefix'     => 'fatorcms',
            'fatorcms'   => true
        )
    );

    Router::connect("/fatorcms/reservas",
        array(
            'plugin'     => 'fd_imoveis',
            'controller' => 'fd_reservas',
            'prefix'     => 'fatorcms',
            'action'     => 'index',
            'fatorcms'   => true
        )
    );

    Router::connect("/fatorcms/reservas/:action/*",
        array(
            'plugin'     => 'fd_imoveis',
            'controller' => 'fd_reservas',
            'prefix'     => 'fatorcms',
            'fatorcms'   => true
        )
    );