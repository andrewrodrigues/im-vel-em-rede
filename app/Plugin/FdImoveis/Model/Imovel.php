<?php

    class Imovel extends FdImoveisAppModel
    {

        public $useTable = 'imoveis';

        public $validate = array();

        public $hasMany = array(
            'ColunaImovelValor' => array(
                'className'  => 'ColunaImovelValor',
                'foreignKey' => 'imovel_id',
                'order'      => array('ColunaImovelValor.ordem ASC')
            )
        );

        public function destroy($id)
        {
            $data = parent::find('first', array('conditions' => array('Projeto.id' => $id)));
            if (!$data)
                return false;
            else {
                $update['Projeto']['id'] = $id;
                $update['Projeto']['deleted'] = date('Y-m-d H:i:s');
                if (parent::save($update))
                    return true;
                else {
                    return false;
                }
            }
        }

        public function getEdit($id, $colunas)
        {
            $return = array();
            foreach ($colunas as $coluna) {
                if ($coluna['Coluna']['alias'] == 'nome') {
                    $valor = $this->find('first', array('recursive' => '-1', 'conditions' => array('Imovel.id' => $id)));
                    $return['Imovel'][$coluna['Coluna']['alias']] = $valor['Imovel']['nome'];
                } else {
                    $query = 'SELECT * FROM coluna_imovel_valors AS ColunaImovelValor WHERE coluna_id = "' . $coluna['Coluna']['id'] . '" AND imovel_id = "' . $id . '"';
                    $valor = $this->query($query);
                    if (isset($valor) && $valor)
                        $return['Imovel'][$coluna['Coluna']['alias']] = $valor[0]['ColunaImovelValor']['valor'];
                }
            }
            return $return;
        }

    }