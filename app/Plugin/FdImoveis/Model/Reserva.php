<?php

    class Reserva extends FdImoveisAppModel
    {

        public $useTable = 'reservas';

        public $belongsTo = array(
            'Imovel'   => array(
                'className'  => 'Imoveis',
                'foreignKey' => 'imovel_id'
            ),
            'Projeto'  => array(
                'className'  => 'Projeto',
                'foreignKey' => 'projeto_id'
            ),
            'Corretor' => array(
                'className'  => 'Usuario',
                'foreignKey' => 'usuario_id'
            )
        );

        public function verifica_reserva($imovel_id)
        {
            return $this->find('first',
                array(
                    'conditions' => array(
                        'Reserva.imovel_id' => $imovel_id
                    )
                )
            );
        }

        public function getTotalDisponivel($projeto_id)
        {
            App::import('Model', 'FdImoveis.ColunaImovelValor');
            $this->ColunaImovelValor = new ColunaImovelValor();

            App::import('Model', 'FdImoveis.Imovel');
            $this->Imovel = new Imovel();

            $query = $this->Imovel->find('list',
                array(
                    'recursive'  => -1,
                    'conditions' => array(
                        'Imovel.projeto_id' => $projeto_id
                    )
                )
            );

            $count = $this->ColunaImovelValor->find('count',
                array(
                    'conditions' => array(
                        'ColunaImovelValor.coluna_id' => 4,
                        'ColunaImovelValor.imovel_id' => $query,
                        'ColunaImovelValor.valor'     => 1
                    )
                )
            );

            return $count;
        }

        public function getTotalReservados($projeto_id)
        {
            App::import('Model', 'FdImoveis.ColunaImovelValor');
            $this->ColunaImovelValor = new ColunaImovelValor();

            App::import('Model', 'FdImoveis.Imovel');
            $this->Imovel = new Imovel();

            $query = $this->Imovel->find('list',
                array(
                    'recursive'  => -1,
                    'conditions' => array(
                        'Imovel.projeto_id' => $projeto_id
                    )
                )
            );

            $count = $this->ColunaImovelValor->find('count',
                array(
                    'conditions' => array(
                        'ColunaImovelValor.coluna_id' => 4,
                        'ColunaImovelValor.imovel_id' => $query,
                        'ColunaImovelValor.valor'     => 2
                    )
                )
            );

            return $count;

        }

        public function removeReserva($data)
        {
            $reservaExists = $this->find('first',
                array(
                    'conditions' => array(
                        'Reserva.imovel_id'      => $data['ImovelVenda']['imovel_id'],
                        'Reserva.construtora_id' => $data['ImovelVenda']['construtora_id'],
                        'Reserva.projeto_id'     => $data['ImovelVenda']['projeto_id'],
                        'Reserva.deleted IS NULL'
                    )
                )
            );
            if ($reservaExists) {
                $this->id = $reservaExists['Reserva']['id'];
                $this->saveField('deleted', date('Y-m-d H:i:s'));
            }
        }

    }