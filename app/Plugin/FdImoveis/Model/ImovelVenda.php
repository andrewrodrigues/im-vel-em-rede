<?php

    class ImovelVenda extends FdImoveisAppModel
    {

        public $useTable = 'imovel_vendas';

        public $virtualFields = array(
            'totalCorretorVenda' => 'SELECT COUNT(Iv.id) FROM imovel_vendas AS Iv WHERE Iv.deleted IS NULL'
        );

        public $belongsTo = array(
            'Corretor'    => array(
                'className'  => 'Usuario',
                'foreignKey' => 'corretor_id'
            ),
            'Construtora' => array(
                'className'  => 'Usuario',
                'foreignKey' => 'construtora_id'
            ),
            'Projeto'     => array(
                'className'  => 'Projeto',
                'foreignKey' => 'projeto_id'
            ),
            'Imovel'      => array(
                'className'  => 'FdImoveis.Imovel',
                'foreignKey' => 'imovel_id'
            ),
            'Imobiliaria' => array(
                'className'  => 'Imobiliaria',
                'foreignKey' => 'imobiliaria_id'
            )
        );

        public function destroy($id)
        {
            $data = parent::find('first', array('conditions' => array('ImovelVenda.id' => $id)));
            if (!$data)
                return false;
            else {
                $update['ImovelVenda']['id'] = $id;
                $update['ImovelVenda']['deleted'] = date('Y-m-d H:i:s');
                if (parent::save($update))
                    return true;
                else {
                    return false;
                }
            }
        }
    }