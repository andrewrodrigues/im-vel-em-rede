<?php

    class ColunaImovelValor extends FdImoveisAppModel
    {

        public $belongsTo = array(
            'Coluna' => array(
                'className'  => 'Coluna',
                'foreignKey' => 'coluna_id'
            )
        );

        public function getValue($imovel_id, $coluna_id)
        {
            $query = 'SELECT * FROM coluna_imovel_valors AS ColunaImovelValor WHERE imovel_id = "' . $imovel_id . '" AND coluna_id = "' . $coluna_id . '"';
            $query = $this->query($query);
            if (isset($query[0]['ColunaImovelValor']))
                return $query[0];
            return false;
        }

    }