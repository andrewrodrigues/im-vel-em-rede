<?php

    class Imobiliaria extends FdImoveisAppModel
    {

        public $virtualFields = array(
            'totalVendidos' => 'SELECT COUNT(id) FROM imovel_vendas IV WHERE IV.imobiliaria_id = Imobiliaria.id AND IV.deleted IS NULL'
        );

        public $hasMany = array(
            'ImovelVenda' => array(
                'className'  => 'ImovelVenda',
                'foreignKey' => 'imobiliaria_id',
                'conditions' => array(
                    'ImovelVenda.deleted IS NULL'
                )
            )
        );

        public $belongsTo = array(
            'Usuario' => array(
                'className'  => 'Usuario',
                'foreignKey' => 'usuario_id'
            )
        );

    }