<?php

    class Coluna extends FdImoveisAppModel
    {

        public $validate = array(
            'nome' => array(
                'notempty' => array(
                    'rule'    => array('notempty'),
                    'message' => 'Campo de preenchimento obrigatório.',
                )
            )
        );

        public function getColunas($id)
        {
            return $this->find('all',
                array(
                    'conditions' => array(
                        'OR' => array(
                            array('Coluna.projeto_id' => $id),
                            array('Coluna.projeto_id' => 0)
                        )
                    ),
                    'order'      => array('Coluna.ordem ASC')
                )
            );
        }

        public function getColuna($alias)
        {
            if (AuthComponent::user('master') || AuthComponent::user('constructor'))
                $query = 'SELECT * FROM colunas AS Coluna WHERE alias = "'.$alias.'" LIMIT 1';
            else {
                $query = 'SELECT * FROM colunas AS Coluna WHERE alias = "'.$alias.'" AND privado = 0 LIMIT 1';
            }
            $query = $this->query($query);
            return isset($query[0]) ? $query[0] : false;
        }


        public function colunasQuantidade($alias)
        {
            $query = $this->find('count',
                array(
                    'recursive' => -1,
                    'conditions' => array(
                        'Coluna.alias' => $alias
                    )
                )
            );
            if ($query > 0) {
                return true;
            } else {
                return false;
            }
        }
    }