<?php
    class FdReservasController extends FdImoveisAppController
    {

        public $uses = array('FdImoveis.Reserva');

        public function fatorcms_index()
        {
            if (!AuthComponent::user('master')) {
                $data = $this->Reserva->find('all',
                    array(
                        'recursive'  => -1,
                        'conditions' => array(
                            'Reserva.construtora_id' => AuthComponent::user('id')
                        )
                    )
                );
            } else {
                $data = $this->Reserva->find('all');
            }
            $this->set(compact('data'));
        }

        public function fatorcms_show($reserva_id = 0, $imovel_id = null)
        {
            if (is_null($imovel_id)) {
                $data = $this->Reserva->find('first',
                    array(
                        'conditions' => array(
                            'Reserva.id' => $reserva_id
                        )
                    )
                );
            } else {
                $data = $this->Reserva->find('first',
                    array(
                        'conditions' => array(
                            'Reserva.imovel_id' => $imovel_id
                        )
                    )
                );
            }
            $this->set('referer', $this->referer());
            $this->set(compact('data'));
        }

    }