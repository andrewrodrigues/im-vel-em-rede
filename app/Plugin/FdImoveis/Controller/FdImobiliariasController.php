<?php


    class FdImobiliariasController extends FdImoveisAppController
    {

        public $uses = array(
            'FdImoveis.Imobiliaria'
        );

        public function fatorcms_index()
        {
            if (AuthComponent::user('master')) {
                $data = $this->Imobiliaria->find('all', array('recursive' => 1, 'order' => array('Imobiliaria.id DESC')));
            } else {
                $data = $this->Imobiliaria->find('all', array('conditions' => array('Imobiliaria.usuario_id' => AuthComponent::user('id')), 'order' => array('Imobiliaria.id DESC')));
            }
            $this->set(compact('data'));
        }

        public function fatorcms_add()
        {
            if ($this->request->is('post')) {
                if (empty($this->request->data['Imobiliaria']['nome'])) {
                    $this->Session->setFlash('Campo nome é obrigatório', 'fatorcms_danger');
                    $this->redirect($this->referer());
                }
                $this->request->data['Imobiliaria']['slug'] = Inflector::slug($this->request->data['Imobiliaria']['nome'], '-');
                $this->request->data['Imobiliaria']['usuario_id'] = AuthComponent::user('id');

                $exists = $this->Imobiliaria->findBySlug($this->request->data['Imobiliaria']['slug']);

                if (!$exists) {
                    if ($this->Imobiliaria->save($this->request->data)) {
                        $this->Session->setFlash('Imobiliária ' . $this->request->data['Imobiliaria']['nome'] . ', foi salva com sucesso.', 'fatorcms_success');
                        $this->redirect('/fatorcms/imobiliarias');
                    } else {
                        $this->Session->setFlash('Ops, não foi possível salvar a imobiliária, tente novamente.', 'fatorcms_danger');
                        $this->redirect($this->referer());
                    }
                } else {
                    $this->Session->setFlash('A imobiliária já está cadastrada.', 'fatorcms_success');
                    $this->redirect($this->referer());
                }
            }
        }

        public function fatorcms_edit($id)
        {
            if (empty($id)) {
                $this->Session->setFlash('Ops, nenhum registro encontrado.', 'fatorcms_danger');
                $this->redirect($this->referer());
            }
            if ($this->request->is('put')) {
                if (empty($this->request->data['Imobiliaria']['nome'])) {
                    $this->Session->setFlash('Campo nome é obrigatório', 'fatorcms_danger');
                    $this->redirect($this->referer());
                }

                $this->request->data['Imobiliaria']['id'] = $id;
                $this->request->data['Imobiliaria']['slug'] = Inflector::slug($this->request->data['Imobiliaria']['nome'], '-');
                $this->request->data['Imobiliaria']['usuario_id'] = AuthComponent::user('id');

                $exists = $this->Imobiliaria->findBySlug($this->request->data['Imobiliaria']['slug']);

                if (!$exists) {
                    if ($this->Imobiliaria->save($this->request->data)) {
                        $this->Session->setFlash('Imobiliária ' . $this->request->data['Imobiliaria']['nome'] . ', foi salva com sucesso.', 'fatorcms_success');
                        $this->redirect('/fatorcms/imobiliarias');
                    } else {
                        $this->Session->setFlash('Ops, não foi possível salvar a imobiliária, tente novamente.', 'fatorcms_danger');
                        $this->redirect($this->referer());
                    }
                } else {
                    $this->Session->setFlash('A imobiliária já está cadastrada.', 'fatorcms_success');
                    $this->redirect($this->referer());
                }
            }
            $this->data = $this->Imobiliaria->findById($id);
        }

        public function fatorcms_deletar($id)
        {
            if (!empty($id)) {
                $exists = $this->Imobiliaria->findById($id);
                if ($exists) {
                    $this->Imobiliaria->id = $id;
                    $this->Imobiliaria->saveField('deleted', date('Y-m-d H:i:s'));
                    $this->Session->setFlash('Imobiliária '.$exists['Imobiliaria']['nome'].' foi deletada com sucesso', 'fatorcms_success');
                }
            }
            $this->redirect($this->referer());
        }

    }