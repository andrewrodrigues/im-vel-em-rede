<?php

App::uses('CakeEmail', 'Network/Email');

class SendEmailsComponent extends Component {

    public $controller = null;
	public $email = '';
	public $templates = array();

	public function initialize(Controller $controller){
	    $this->controller = $controller;

	    $this->templates = $this->get_templates();
	}

	private function get_templates(){
		if (Cache::read('templates') === false) {
			App::import('Model', 'FdEmails.Template');
			$this->Template = new Template();
            $templates = $this->Template->find('list', array(
								             	   'fields' => array(
								                	    'Template.slug',
								                   	 	'Template.conteudo',
								                	),
								                	'conditions' => array('status' => true)
		            							)
											);
            Cache::write('templates', $templates);
        } else {
            $templates = Cache::read('templates');
        }

        return $templates;
	}

	private function set_log($to, $subject, $content, $status){
		//BEGIN: salva log de e-mails
		App::import('Model', 'FdEmails.Email');
		$this->Email = new Email();

		$log['id'] 		= null;
		$log['to'] 	 	= $to;
		$log['subject'] = $subject;
		$log['content'] = $content;
		$log['status']  = $status;
		$this->Email->save($log);
	}

    public function send($to, $subject = null, $content) {

    	if(is_null($subject)){
    		$subject = 'Contato realizado pelo site';
    	}

        $this->email = new CakeEmail('smtp');
        $this->email->emailFormat('html');
        $ccs = explode(';', Configure::read('Site.Email.CC'));
        $bcc = array();
        foreach($ccs as $cc){
            if($cc != ""){
                // if(Validation::email($cc, true)){
                    $bcc[] = trim($cc);
                // }
            }
        }
        $this->email->bcc($bcc);

        $to = ($_SERVER['SERVER_NAME'] == "portal.uniritter.local") ? Configure::read('Site.Email.ToDev') : $to;

		$this->email->to($to);
		$this->email->subject($subject);

		$status = null;
		if($this->email->send($content)){
			$status = true;
		}else{
			$status = false;
		}

        // set log
		$this->set_log(json_encode($to), $subject, $content, $status);

		return $status;
    }

    public function _enviaSacCampus($data){
    	App::import("helper", "String");
        $this->String = new StringHelper(new View(null));

        $content_email = str_replace(
                        array(
                        	'{SITE_NOME}',
                        	'{SITE_URL}',
                        	'{DATA}',
                        	'{CAMPUS}',
                        	'{NOME}',
                        	'{EMAIL}',
                        	'{TELEFONE}',
                        	'{MENSAGEM}'
                        ),
                        array(
                        	Configure::read('Site.Nome'),
                        	Router::url('/', true),
                        	date('d/m/Y H:i:s'),
                        	$data['campus_nome'],
                            $data['nome'],
                            $data['email'],
                            $data['telefone'],
                            $data['mensagem']
                        ), $this->templates['duvida']
                    );

        App::import('Model', 'FdSac.SacTipo');
        $this->SacTipo = new SacTipo();;
        $sac_tipo = $this->SacTipo->find('first', array('conditions' => array('SacTipo.id' => $data['sac_tipo_id'])));
        if(!empty($sac_tipo) && $sac_tipo['SacTipo']['email'] != ""){
        	if(count(explode(';', $sac_tipo['SacTipo']['email'])) > 1){
                foreach (explode(';', $sac_tipo['SacTipo']['email']) as $key => $value) {
                    // if(Validation::email($value, true)){
                        $to[] = $value;
                    // }
                }
                return $this->send($to, null, $content_email);
            }else{
                return $this->send($sac_tipo['SacTipo']['email'], null, $content_email);
            }
        }
    }

    public function _enviaSac($data){
        App::import("helper", "String");
        $this->String = new StringHelper(new View(null));

        $content_email = str_replace(
                        array(
                            '{SITE_NOME}',
                            '{SITE_URL}',
                            '{DATA}',
                            '{QUEM_EH}',
                            '{NOME}',
                            '{EMAIL}',
                            '{TELEFONE}',
                            '{MENSAGEM}'
                        ),
                        array(
                            Configure::read('Site.Nome'),
                            Router::url('/', true),
                            date('d/m/Y H:i:s'),
                            $this->String->getSacTipoLead($data['lead_tipo']),
                            $data['nome'],
                            $data['email'],
                            $data['telefone'],
                            $data['mensagem']
                        ), $this->templates['fale_conosco']
                    );

        //solicitação do cliente
        //na task: http://project.fatordigital.com.br/projects/192/tasks/70
        /*
        Aluno: nra@uniritter.edu.br
        Professor: prograd@uniritter.edu.br
        Funcionário: rh@uniritter.edu.br
        Público: pedro.montiel@fadergs.edu.br; andressa_lima@uniritter.edu.br
        // TODO: DEIXAR ISSO DINÂMICO NO BANCO ASSIM QUE DER
        */

        if(isset($data['lead_tipo'])){
            if(preg_match('/uniritter.edu.br/', $_SERVER['HTTP_HOST'])){
                $emails['aluno']        = 'nra@uniritter.edu.br';
                $emails['professor']    = 'prograd@uniritter.edu.br';
                $emails['funcionario']  = 'rh@uniritter.edu.br';
                $emails['publico']      = 'pedro.montiel@fadergs.edu.br;andressa_lima@uniritter.edu.br';
            }else{
                $emails['aluno']        = 'cleber.alves+aluno@fatordigital.com.br';
                $emails['professor']    = 'cleber.alves+professor@fatordigital.com.br';
                $emails['funcionario']  = 'cleber.alves+funcionario@fatordigital.com.br';
                $emails['publico']      = 'cleber.alves+publico1@fatordigital.com.br;cleber.alves+publico2@fatordigital.com.br';
            }

            if(isset($emails[$data['lead_tipo']])){
                $email_to = $emails[$data['lead_tipo']];

                if($email_to != ""){
                    if(count(explode(';', $email_to)) > 1){
                        foreach (explode(';', $email_to) as $key => $value) {
                            // if(Validation::email($value, true)){
                                $to[] = trim($value);
                            // }
                        }
                        return $this->send($to, null, $content_email);
                    }else{
                        return $this->send($email_to, null, $content_email);
                    }
                }
            }
        }else{
            App::import('Model', 'FdSac.SacTipo');
            $this->SacTipo = new SacTipo();

            $sac_tipo = $this->SacTipo->find('first', array('conditions' => array('SacTipo.id' => $data['sac_tipo_id'])));
            if(!empty($sac_tipo) && $sac_tipo['SacTipo']['email'] != ""){
                if(count(explode(';', $sac_tipo['SacTipo']['email'])) > 1){
                    foreach (explode(';', $sac_tipo['SacTipo']['email']) as $key => $value) {
                        // if(Validation::email($value, true)){
                            $to[] = trim($value);
                        // }
                    }
                    return $this->send($to, null, $content_email);
                }else{
                    return $this->send($sac_tipo['SacTipo']['email'], null, $content_email);
                }
            }
        }
        
    }
	
	public function _enviaSacInterna($data){
        App::import("helper", "String");
        $this->String = new StringHelper(new View(null));



        App::import('Model', 'FdRotas.Rota');
        $this->Rota = new Rota();
        $rota = $this->Rota->find('first', array('recursive' => -1, 'conditions' => array('Rota.seo_url' => $data['url'], 'Rota.sites LIKE' => "%" . Configure::read('site') . "%")));

        App::import('Model', 'Fd' . $rota['Rota']['model']. 's.' . $rota['Rota']['model']);
        $this->{$rota['Rota']['model']} = new $rota['Rota']['model']();
        $dados_rota = $this->{$rota['Rota']['model']}->find('first', array('recursive' => -1, 'conditions' => array('id' => $rota['Rota']['row_id'])));

        //begin validacao para a rota de noticias
            if(!empty($rota)){
                if($rota['Rota']['model'] == 'Noticia' && !empty($dados_rota)){
                    if($dados_rota[$rota['Rota']['model']]['noticia_tipo_id'] != ""){
                        App::import('Model', 'FdNoticias.NoticiaTipo');
                        $this->NoticiaTipo = new NoticiaTipo();
                        $noticia_tipo = $this->NoticiaTipo->find('first', array('recursive' => -1, 'conditions' => array('NoticiaTipo.id' => $dados_rota[$rota['Rota']['model']]['noticia_tipo_id'])));
                        if(!empty($noticia_tipo)){
                            $rota['Rota']['model'] = 'NoticiaTipo';
                            $dados_rota = $noticia_tipo;
                        }
                    }
                }
            }
        //end validacao para a rota de noticias

        $content_email = str_replace(
                        array(
                            '{SITE_NOME}',
                            '{SITE_URL}',
                            '{DATA}',
                            '{LOCAL}',
                            '{URL}',
                            '{NOME}',
                            '{EMAIL}',
                            '{TELEFONE}',
                            '{MENSAGEM}'
                        ),
                        array(
                            Configure::read('Site.Nome'),
                            Router::url('/', true),
                            date('d/m/Y H:i:s'),
                            (!empty($dados_rota)) ? $rota['Rota']['model'] . ': ' . $dados_rota[$rota['Rota']['model']]['nome'] : 'Geral',
                            Router::url('/' . $data['url'], true),
                            $data['nome'],
                            $data['email'],
                            $data['telefone'],
                            $data['mensagem']
                        ), $this->templates['fale_conosco_interna']
                    );

        App::import('Model', 'FdSac.SacTipo');
        $this->SacTipo = new SacTipo();
        $sac_tipo = $this->SacTipo->find('first', array('conditions' => array('SacTipo.id' => $data['sac_tipo_id'])));
        if(!empty($sac_tipo) && $sac_tipo['SacTipo']['email'] != ""){
            $to = array();
            if(count(explode(';', $sac_tipo['SacTipo']['email'])) > 1){
                foreach (explode(';', $sac_tipo['SacTipo']['email']) as $key => $value) {
                    if($value != ""){
                        $to[] = trim($value);
                    }
                }                
            }else{
                $to[] = trim($sac_tipo['SacTipo']['email']);
            }

            if(!empty($rota)){
                
                if(!empty($dados_rota)){
                    if(!empty($dados_rota) && $dados_rota[$rota['Rota']['model']]['formulario_email'] != ""){
                        $to = array();
                        if(count(explode(';', $dados_rota[$rota['Rota']['model']]['formulario_email'])) > 1){
                            foreach (explode(';', $dados_rota[$rota['Rota']['model']]['formulario_email']) as $key => $value) {
                                if($value != ""){
                                    $to[] = trim($value);
                                }
                            }                
                        }else{
                            $to[] = trim($dados_rota[$rota['Rota']['model']]['formulario_email']);
                        }
                    }

                }
            }

            return $this->send($to, null, $content_email);
        }
    }

    public function _enviaSacProfessor($data){
        App::import("helper", "String");
        $this->String = new StringHelper(new View(null));

        $content_email = str_replace(
                        array(
                            '{SITE_NOME}',
                            '{SITE_URL}',
                            '{DATA}',
                            '{PROFESSOR}',
                            '{CURSO}',
                            '{NOME}',
                            '{EMAIL}',
                            '{MENSAGEM}'
                        ),
                        array(
                            Configure::read('Site.Nome'),
                            Router::url('/', true),
                            date('d/m/Y H:i:s'),
                            $data['professor_nome'],
                            $data['curso_nome'],
                            $data['nome'],
                            $data['email'],
                            $data['mensagem']
                        ), $this->templates['contato_professor']
                    );

        if(isset($data['professor_email_composto']) && $data['professor_email_composto'] == true && count($data['professor_email']) > 0){
            foreach ($data['professor_email'] as $key => $email) {
                $this->send($email, null, $content_email);
            }
        }else{
            return $this->send($data['professor_email'], null, $content_email);
        }
    }
	
	public function _enviaSacInternational($data){
        App::import("helper", "String");
        $this->String = new StringHelper(new View(null));
		
		$destinos = '---';
		
		if($data['destinos'] != "" && is_array($data['destinos']) && count($data['destinos']) > 0){
			$destinos = '';
			foreach($data['destinos'] as $k => $destino){
				if(is_array($destino)){
					foreach($destino as $dest){
						$destinos .= $this->String->getDestinoIntercambio($dest).'<br />';
					}
				}else{
					$destinos .= $this->String->getDestinoIntercambio($destino).'<br />';
				}
			}
		}

        $content_email = str_replace(
                        array(
                            '{SITE_NOME}',
                            '{SITE_URL}',
                            '{DATA}',
                            '{NOME}',
                            '{CURSO}',
                            '{SEMESTRE}',
                            '{DATA_NASCIMENTO}',
							'{CELULAR}',
							'{EMAIL}',
							'{INTERCAMBIO}',
							'{INTERCAMBIO_LOCAL}',
							'{DESTINOS}',
							'{DESTINO_OUTRO}'
                        ),
                        array(
                            Configure::read('Site.Nome'),
                            Router::url('/', true),
                            date('d/m/Y H:i:s'),
                            $data['nome'],
                            $data['curso'],
                            $data['semestre'],
                            $data['data_nascimento'],
							$data['celular'],
							$data['email'],
							($data['intercambio'] == true) ? 'Sim' : 'Não',
							($data['intercambio_local'] != "") ? $data['intercambio_local'] : '---',
							$destinos,
							($data['destino_outro'] != "") ? $data['destino_outro'] : '---',
                        ), $this->templates['international_office']
                    );

        App::import('Model', 'FdSac.SacTipo');
        $this->SacTipo = new SacTipo();;
        $sac_tipo = $this->SacTipo->find('first', array('conditions' => array('SacTipo.id' => $data['sac_tipo_id'])));
        if(!empty($sac_tipo) && $sac_tipo['SacTipo']['email'] != ""){
            if(count(explode(';', $sac_tipo['SacTipo']['email'])) > 1){
                foreach (explode(';', $sac_tipo['SacTipo']['email']) as $key => $value) {
                    // if(Validation::email($value, true)){
                        $to[] = $value;
                    // }
                }
                return $this->send($to, null, $content_email);
            }else{
                return $this->send($sac_tipo['SacTipo']['email'], null, $content_email);
            }
        }
    }
}
