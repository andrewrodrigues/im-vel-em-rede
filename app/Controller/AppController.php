<?php
    /**
     * Application level Controller
     *
     * This file is application-wide controller file. You can put all
     * application-wide controller-related methods here.
     *
     * PHP 5
     *
     * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
     * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
     *
     * Licensed under The MIT License
     * For full copyright and license information, please see the LICENSE.txt
     * Redistributions of files must retain the above copyright notice.
     *
     * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
     * @link          http://cakephp.org CakePHP(tm) Project
     * @package       app.Controller
     * @since         CakePHP(tm) v 0.2.9
     * @license       http://www.opensource.org/licenses/mit-license.php MIT License
     */
    App::uses('Controller', 'Controller');

    /**
     * Application Controller
     *
     * Add your application-wide methods in the class below, your controllers
     * will inherit them.
     *
     * @package        app.Controller
     * @link           http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
     */
    class AppController extends Controller
    {


        public $components = array(
            'Acl',
            'Auth',
            'Session',
            'Paginator',
            'ImportDb',
            'Reports',
            'Cookie',
            'Acl.AclConfig',
            'Common',
            'FilterResults.FilterResults' => array(
                'auto'    => array(
                    'paginate' => false,
                    'explode'  => true,
                ),
                'explode' => array(
                    'character'   => ' ',
                    'concatenate' => 'AND',
                ),
            ),
            'FdMenus.Menuu',
            'FdEmails.SendEmails',
        );

        public $helpers = array(
            'Session',
            'Time',
            'String',
            'FilterResults.FilterForm' => array(
                'operators' => array(
                    'LIKE'       => 'containing',
                    'NOT LIKE'   => 'not containing',
                    'LIKE BEGIN' => 'starting with',
                    'LIKE END'   => 'ending with',
                    '='          => 'equal to',
                    '!='         => 'different',
                    '>'          => 'greater than',
                    '>='         => 'greater or equal to',
                    '<'          => 'less than',
                    '<='         => 'less or equal to'
                )
            ),
            'Html'                     => array('className' => 'MyHtml'),
            'FdMenus.Menus',
        );

        /**
         * Migalha
         *
         * @var array
         * @access protected
         */
        protected $_breadcrumb = array();

        /**
         * Adição de migalha
         *
         *
         * @param string $nome Nome da migalha
         * @param mixed  $url  Url de acesso (opcional). Deve seguir o padrão do cake para url
         * @access protected
         * @return AppController Próprio Objeto para encadeamento
         */
        protected function _addBreadcrumb($nome, $url = null)
        {
            if (empty($url)) {
                $url = 'javascript:void(0);';
            }
            $this->_breadcrumb[] = array('nome' => $nome, 'url' => $url);
            return $this;
        }

        /**
         * Mêtodo setUrlReferer
         *
         * @return Array
         */
        protected function _setUrlReferer()
        {
            $this->Common->setUrlReferer();
        }

        /**
         * Mêtodo _resetCaches
         *
         * @return Array
         */
        protected function _resetCaches()
        {
            Cache::clearGroup('site');
        }

        /**
         * Mêtodo _saveStatus
         *
         * @return Array
         */
        protected function _saveStatus($model, $id, $status)
        {
            return $this->Common->saveStatus($model, $id, $status);
        }

        /**
         * Mêtodo _redirectFilter
         * responsavel por gerenciar os redirects dos filters
         * @return Array
         */
        protected function _redirectFilter($referer = null)
        {
            return $this->Common->redirectFilter($referer);
        }

        // sobrescrita de gancho para antes da execução das filtragens
        public function beforeFilter()
        {
            // Configue ACL
            $this->AclConfig->configureAcl();

            // Set user logado
            $this->set('auth', $this->Auth->user());

            if (!$this->request->is('ajax')) {

                //altero o layout se o usuário estiver no admin
                if ($this->Common->isAdminMode()) {
                    //layout
                    $this->layout = 'fatorcms';
                    //menu
                    $this->set('menus_for_layout', $this->Menuu->carregaMenu(array('admin')));
                } else {
                    //layout
                    $this->layout = 'default';
                    //menu
                    $menus_for_layout = $this->Menuu->carregaMenu(array('site_institucional', 'site_footer'));
                    $this->set('menus_for_layout', $menus_for_layout);
                }
            }
        }

        // sobrescrita de gancho para antes da renderização da view
        public function beforeRender()
        {
            // envia a migalha para a visualização
            $this->set('breadcrumbs', $this->_breadcrumb);
        }

    }
