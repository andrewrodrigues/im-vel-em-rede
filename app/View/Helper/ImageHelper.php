<?php
App::uses('AppHelper', 'View/Helper');

class ImageHelper extends AppHelper {

	public $helpers = array('Html');

/**
 * Default Constructor
 *
 * @param View $View The View this helper is being attached to.
 * @param array $settings Configuration settings for the helper.
 */
	public function __construct(View $View, $settings = array()) {
		parent::__construct($View, $settings);
	}

/**
 *
 * @param string $path Path to the image file
 * @param array $options
 *
 * @access public
 */
	public function show($path, $options = array()) {
		$options = array_merge(array(
			'width'				=> null,	//Width of the new Image, Default is Original Width
			'height'			=> null,	//Height of the new Image, Default is Original Height
		), $options);

		if(stripos('http', $path) === true){
			$url = array_reverse(explode('/', $path));
			$path = $url[0];
		}

		$fullpath = ROOT . '/' . APP_DIR . '/' . WEBROOT_DIR . '/' . $path;
		if (!file_exists($fullpath) || !is_file($fullpath)) {
			$path = '/img/common/imagem-indisponivel.jpg';
		}

		if(stripos('http', $path) === false){
			$new_path = $this->Html->Url('/'.$path, true);
		}

		return $this->Html->image($new_path,$options);
	}
}
