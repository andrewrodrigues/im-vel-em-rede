<?php
App::uses('Helper', 'View');

/**
 * Application helper
 *
 * Add your application-wide methods in the class below, your helpers
 * will inherit them.
 *
 * @package       app.View.Helper
 */
class StringHelper extends Helper {

	const STATUS_ATIVO = 'Ativo';
	const STATUS_INATIVO = 'Inativo'; 

	public function getStatus($valor) {
	    switch ($valor) {
			case '1':
				return '<span class="btn btn-round btn-xs btn-primary">' . self::STATUS_ATIVO . '</span>';
				break;

			default:
				return '<span class="btn btn-round btn-xs btn-danger">' . self::STATUS_INATIVO . '</span>';
				break;
		}
	}

    public function totalDisponivel($projeto_id)
    {
        App::import('Model', 'FdImoveis.Reserva');
        $this->Reserva = new Reserva();
        return $this->Reserva->getTotalDisponivel($projeto_id);
    }

    public function getImobiliaria($id, $key = false)
    {
        App::import('Model', 'FdImoveis.Imobiliaria');
        $imobi = new Imobiliaria();
        $return = $imobi->findById($id);
        if ($key && $return)
            return $return['Imobiliaria'][$key];
        else {
            return $return;
        }
    }

	public function getStatusEvento($valor) {
	    switch ($valor) {
			case 'aberto':
				return 'Aguardando aprovação';
				break;

			case 'confirmado':
				return 'Aprovado';
				break;

			case 'cancelado':
				return 'Não Aprovado';
				break;
		}
	}

	public function getSacTipoLead($valor) {
	    switch ($valor) {
			case 'aluno':
				return 'Aluno';
				break;

			case 'professor':
				return 'Professor';
				break;

			case 'funcionario':
				return 'Funcionário';
				break;

			case 'publico':
				return 'Público Externo';
				break;
		}
	}

	//SEMPRE manter uma chave diferente (dificilmente será alterado, pois a ideia é haver apenas portal e hotsite)
	public function getSites() {
	    return array(
	    			'portal' 			=> 'Portal', 
	    			);
	}

	public function getBtnBySite($site){
		switch ($site) {
			case 'portal':
				return '<span class="btn btn-xs btn-success">' . $site . '</span>';
				break;
		}
	}
	
	public function getDestinoIntercambio($valor) {
	    switch ($valor) {
			case 'alemanha':
				return 'Alemanha';
				break;

			case 'irlanda':
				return 'Irlanda';
				break;
		}
	}
	
	public function getDestinoIntercambioList() {
	    return array('alemanha' => 'Alemanha', 'irlanda' => 'Irlanda');
	}

	/**
	* Formata data para a string date passada
	*
	* @param string Formato (utilizar nota��o do PHP), se n�o informada gera DateTime Sem Timezone
	* @param string Data
	* @param string Data relativa (quando necess�rio para c�lculo, como "pr�xima segunda")
	*/
	public function DataFormatada($formato=null,$data=null,$relative=null){
		if(is_null($data)) $data = time();
		if(is_null($formato)) $formato = 'Y-m-d H:i:s';
		return date($formato,$this->DataToTime($data,$relative));
	}

	/**
	* Retorna o timestamp da data informada, é uma versão otimizada do strtotime
	*
	* @param string Data
	* @param string Data relativa (quando necessário para cálculo, como "próxima segunda")
	*/
	private function DataToTime($data,$relative=null){
		if(is_numeric($data)) return $data;
		if(preg_match('/([0-9]{1,2})[ \t\n\r]*\/[ \t\n\r]*([0-9]{1,2})[ \t\n\r]*\/[ \t\n\r]*([0-9]{4}|[0-9]{2})[ \t\n\r]*([0-9]{2})[ \t\n\r]*(:|h|hour|hours|horas|hr)[ \t\n\r]*([0-9]{2})[ \t\n\r]*(:|m|min|mins|minutos|minuto|mn)[ \t\n\r]*([0-9]{2})/i',$data,$m)){
			$hora = $m[4]; $minuto = $m[6]; $segundo = $m[8]; $dia = $m[1]; $mes = $m[2]; $ano = $m[3];
		}elseif(preg_match('/([0-9]{1,2})[ \t\n\r]*\/[ \t\n\r]*([0-9]{1,2})[ \t\n\r]*\/[ \t\n\r]*([0-9]{4}|[0-9]{2})[ \t\n\r]*([0-9]{2})[ \t\n\r]*(:|h|hour|hours|horas|hr)[ \t\n\r]*([0-9]{2})[ \t\n\r]*(:|m|min|mins|minutos|minuto|mn)/i',$data,$m)){
			$hora = $m[4]; $minuto = $m[6]; $segundo = 0; $dia = $m[1]; $mes = $m[2]; $ano = $m[3];
		}elseif(preg_match('/([0-9]{1,2})[ \t\n\r]*\/[ \t\n\r]*([0-9]{1,2})[ \t\n\r]*\/[ \t\n\r]*([0-9]{4}|[0-9]{2})[ \t\n\r]*([0-9]{2})[ \t\n\r]*(:|h|hour|hours|horas|hr)/i',$data,$m)){
			$hora = $m[4]; $minuto = 0; $segundo = 0; $dia = $m[1]; $mes = $m[2]; $ano = $m[3];
		}elseif(preg_match('/([0-9]{1,2})[ \t\n\r]*\/[ \t\n\r]*([0-9]{1,2})[ \t\n\r]*\/[ \t\n\r]*([0-9]{4}|[0-9]{2})/',$data,$m)){
			$hora = 0; $minuto = 0; $segundo = 0; $dia = $m[1]; $mes = $m[2]; $ano = $m[3];
		}elseif(preg_match('/([0-9]{1,2})[ \t\n\r]*\/[ \t\n\r]*([0-9]{1,2})/',$data,$m)){
			$hora = 0; $minuto = 0; $segundo = 0; $dia = $m[1]; $mes = $m[2]; $ano = date('Y',is_null($relative)?time():$relative);
		}elseif(preg_match('/([0-9]{1,2})[ \t\n\r]*\/[ \t\n\r]*([0-9]{4})/',$data,$m)){
			$hora = 0; $minuto = 0; $segundo = 0; $dia = 1; $mes = $m[1]; $ano = $m[2];
		}else{
			$tr = array(
				'hoje'=>'today','dia'=>'day', 'semana'=>'week', 'hora'=>'hour', 'minuto'=>'minute', 'segundo'=>'second',
				'meses'=>'months','mês'=>'month','mes'=>'month', 'ano'=>'year',
				'proxima'=>'next', 'próxima'=>'next',  'próximo'=>'next',  'próximo'=>'next', 
				'Última'=>'last', 'ultima'=>'last','ultimo'=>'last','Último'=>'last',
				'segunda-feira'=>'monday','segunda'=>'monday','terça-feira'=>'tuesday','terça'=>'tuesday',
				'quarta-feira'=>'wednesday','quarta'=>'wednesday', 'quinta-feira'=>'thursday','quinta'=>'thursday', 
				'sexta-feira'=>'friday','sexta'=>'friday', 'sábado'=>'saturday','sabado'=>'saturday', 'domingo'=>'sunday',
				'janeiro'=>'january', 'jan'=>'january', 'fevereiro'=>'february','fev'=>'february', 
				'março'=>'march','mar'=>'march', 'abril'=>'april', 'abr'=>'april',
				'maio'=>'may','mai'=>'may', 'junho'=>'june', 'jun'=>'june', 
				'julho'=>'july','jul'=>'july', 'agosto'=>'august', 'ago'=>'august', 
				'setembro'=>'september','set'=>'september', 'outubro'=>'october', 'out'=>'october', 
				'novembro'=>'november','nov'=>'november', 'dezembro'=>'december','dez'=>'december',
				'depois de amanhã'=>'+2 day','depois de amanha'=>'+2 day',
				'anteontem'=>'-2 day',
				'amanhã'=>'tomorrow','amanha'=>'tomorrow','ontem'=>'yesterday',' de '=>''
			);
			return strtotime(str_ireplace(array_keys($tr),array_values($tr),$data),is_null($relative)?time():($this->DataToTime($relative)));		
		}
		return mktime($hora,$minuto,$segundo,$mes,$dia,$ano);
	}

	public function diasemana($diasemana) {
		switch($diasemana) {
			case"0": $diasemana = "Domingo";       break;
			case"1": $diasemana = "Segunda-Feira"; break;
			case"2": $diasemana = "Terça-Feira";   break;
			case"3": $diasemana = "Quarta-Feira";  break;
			case"4": $diasemana = "Quinta-Feira";  break;
			case"5": $diasemana = "Sexta-Feira";   break;
			case"6": $diasemana = "Sábado";        break;
			case"7": $diasemana = "Domingo";        break;
		}
		return $diasemana;
	}

	public function mes($dia_mes, $abre = false) {
		switch($dia_mes) {
			case"1": 
				$dia_mes = ($abre == true) ? "JAN" : "Janeiro"; 	
				break;
			case"2": 
				$dia_mes = ($abre == true) ? "FEV" : "Fevereiro";  
				break;
			case"3": 
				$dia_mes = ($abre == true) ? "MAR" : "Março";  	
				break;
			case"4": 
				$dia_mes = ($abre == true) ? "ABR" : "Abril";  	
				break;
			case"5": 
				$dia_mes = ($abre == true) ? "MAI" : "Maio";   	
				break;
			case"6": 
				$dia_mes = ($abre == true) ? "JUN" : "Junho";      
				break;
			case"7": 
				$dia_mes = ($abre == true) ? "JUL" : "Julho";      
				break;
			case"8": 
				$dia_mes = ($abre == true) ? "AGO" : "Agosto";     
				break;
			case"9": 
				$dia_mes = ($abre == true) ? "SET" : "Setembro";   
				break;
			case"10": 
				$dia_mes = ($abre == true) ? "OUT" : "Outubro";   
				break;
			case"11": 
				$dia_mes = ($abre == true) ? "NOV" : "Novembro";  
				break;
			case"12": 
				$dia_mes = ($abre == true) ? "DEZ" : "Dezembro";  
				break;
		}
		return $dia_mes;
	}

	public function truncate_str($str, $maxlen) {
		if ( strlen($str) <= $maxlen ) return $str;

		$newstr = substr($str, 0, $maxlen);
		if ( substr($newstr,-1,1) != ' ' ) 
			$newstr = substr($newstr, 0, strrpos($newstr, " "));

		return $newstr.'...';
	}

	public function mask($val, $mask){
	 	$maskared = '';
	 	$k = 0;
	 	for($i = 0; $i<=strlen($mask)-1; $i++){
	 		if($mask[$i] == '#'){
	 			if(isset($val[$k])){
	 				$maskared .= $val[$k++];
	 			}
	 		}else{
	 			if(isset($mask[$i])){
	 				$maskared .= $mask[$i];
	 			}
	 		}
	 	}
	 	return $maskared;
	}

	public function moedaToBco($valor) {
        return (float) str_replace(",", ".", str_replace(".", "", $valor));
    }

    public function bcoToMoeda($valor, $prec = 2) {
        return number_format($valor, $prec, ",", ".");
    }
}
