<!-- sidebar start-->

<aside>
    <div id="sidebar" class="nav-collapse">
        <!-- sidebar menu start-->
        <div class="leftside-navigation">

            <?php if(isset($menus_for_layout['admin'])): ?>
                <ul class="sidebar-menu" id="nav-accordion">

                    <?php foreach($menus_for_layout['admin']['threaded'] as $link): ?>

                        <?php if(isset($link['children']) && count($link['children']) == 0): ?>
                            <li>
                                <?php 
                                    $class_ativa = '';
                                    if($link['Link']['seo_url'] == 'fatorcms' && $this->params->url == $link['Link']['seo_url']){
                                        $class_ativa = 'active';
                                    }elseif(stripos($this->params->url, $link['Link']['seo_url']) != false){
                                        $class_ativa = 'active';
                                    }
                                ?>

                                <a class="<?php echo $class_ativa; ?>" href="<?php echo $this->Html->Url('/'.$link['Link']['seo_url']); ?>">
                                    <?php if($link['Link']['class'] != ""): ?>
                                        <i class="fa <?php echo $link['Link']['class']; ?>"></i>
                                    <?php endIf; ?>
                                    <span><?php echo $link['Link']['title']; ?></span>
                                </a>
                            </li>
                        <?php else: ?>
                            <li class="sub-menu">
                                <?php 
                                    $class_ativa = '';
                                    foreach($link['children'] as $k => $children){
                                        //if(stripos($this->params->url, $children['Link']['seo_url']) !== false){
                                        if(stripos($this->params->url, str_replace('fatorcms/', '', $children['Link']['seo_url'])) !== false){
                                            $class_ativa = 'active';
                                        }

                                        if(isset($children['children']) && count($children['children']) > 0){
                                            foreach($children['children'] as $k => $chil){
                                                if($link['Link']['title'] == 'Notícias' && (stripos($this->params->url, 'inquietos') != false || stripos($this->params->url, 'social') != false || stripos($this->params->url, 'noticias') != false) ){
                                                    $class_ativa = 'active';
                                                }
                                            }
                                        }
                                    }
                                ?>

                                <a href="javascript:;" class="<?php echo $class_ativa; ?>">
                                    <?php if($link['Link']['class'] != ""): ?>
                                        <i class="fa <?php echo $link['Link']['class']; ?>"></i>
                                    <?php endIf; ?>
                                    <span><?php echo $link['Link']['title']; ?></span>
                                </a>

                                <ul class="sub">
                                    <?php foreach($link['children'] as $k => $children): ?>
                                        <li>
                                            <?php 
                                                $class_ativa = '';
                                                //if(stripos($this->params->url, $children['Link']['seo_url']) !== false){
                                                if(stripos($this->params->url, str_replace('fatorcms/', '', $children['Link']['seo_url'])) !== false){
                                                    $class_ativa = 'active';
                                                }

                                                if(isset($children['children']) && count($children['children']) > 0){
                                                    foreach($children['children'] as $k => $chil){
                                                        if( (stripos($this->params->url, $chil['Link']['seo_url']) !== false) ){
                                                            $class_ativa = 'active';
                                                        }
                                                    }
                                                }
                                            ?>
                                            <a class="<?php echo $class_ativa; ?>" href="<?php echo $this->Html->Url('/'.$children['Link']['seo_url']); ?>" title="<?php echo $children['Link']['title']; ?>"><?php echo $children['Link']['title']; ?></a>


                                            <?php if(isset($children['children']) && count($children['children']) > 0): ?>
                                                <ul class="sub sub3">
                                                    <?php foreach($children['children'] as $k => $children): ?>
                                                        <li>
                                                            <?php 
                                                                $class_ativa = '';
                                                                //if(stripos($this->params->url, $children['Link']['seo_url']) !== false){
                                                                if(stripos($this->params->url, str_replace('fatorcms/', '', $children['Link']['seo_url'])) !== false){
                                                                    $class_ativa = 'active';
                                                                }
                                                                // elseif($link['Link']['title'] == 'Notícias' && (stripos($this->params->url, 'inquietos') != false || stripos($this->params->url, 'social') != false || stripos($this->params->url, 'noticias') != false) ){
                                                                //     $class_ativa = 'active';
                                                                // }
                                                            ?>
                                                            <a class="<?php echo $class_ativa; ?>" href="<?php echo $this->Html->Url('/'.$children['Link']['seo_url']); ?>" title="<?php echo $children['Link']['title']; ?>"><?php echo $children['Link']['title']; ?></a>
                                                        </li>
                                                    <?php endForeach; ?>
                                                </ul>
                                            <?php endIf; ?>
                                        </li>
                                    <?php endForeach; ?>
                                </ul>
                            </li>
                        <?php endIf; ?>
                    <?php endForeach; ?>
                </ul>
            <?php endIf; ?>

            <?php //echo $this->Menus->getMenu('admin'); ?>
        </div>
        <!-- sidebar menu end-->
    </div>
</aside>

<!--sidebar end -->