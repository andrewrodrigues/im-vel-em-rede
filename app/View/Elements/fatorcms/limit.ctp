<div class="dataTables_length limit-content">
	<?php 
		$limits = array();
		$limits[$this->Paginator->url(array('limit' => '20'), false)] = 20; 
		$limits[$this->Paginator->url(array('limit' => '40'), false)] = 40; 
		$limits[$this->Paginator->url(array('limit' => '60'), false)] = 60; 
		$limits[$this->Paginator->url(array('limit' => '80'), false)] = 80; 

		$selected = array();
		if(isset($this->params->params['named']['limit'])){
			$selected = $this->Paginator->url(array('limit' => $this->params->params['named']['limit']), false);
		}
	?>
	<?php echo $this->Form->input('limit', array(
													'options' => $limits, 
													'selected' => $selected,
													'label' => false, 
													'class' => 'form-control'
												)); ?>
</div>