<!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->charset(); ?>
    <meta data-base="<?php echo $this->Html->Url('/') ?>">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0">
    <meta name="description"
          content="">
    <meta name="author"
          content="">
    <link rel="shortcut icon"
          href="images/favicon.png">
    <title>Imóveis em Rede</title>
    <!--Core CSS -->
    <?php echo $this->Html->css('fatorcms/core/bootstrap3/bootstrap.min') ?>
    <?php echo $this->Html->css('fatorcms/core/bootstrap-datepicker/datepicker'); ?>
    <?php echo $this->Html->css('fatorcms/core/bootstrap-switch'); ?>
    <?php echo $this->Html->css('fatorcms/core/jquery-ui/jquery-ui-1.10.1.custom.min') ?>
    <?php echo $this->Html->css('fatorcms/core/bootstrap-reset') ?>
    <?php echo $this->Html->css('fatorcms/core/font-awesom/css/font-awesome') ?>
    <?php //echo $this->Html->css('fatorcms/core/jvector-map/jquery-jvectormap-1.2.2') ?>
    <?php echo $this->Html->css('fatorcms/core/clndr') ?>

    <?php //echo $this->Html->css('fatorcms/redactor/redactor'); ?>
    <?php echo $this->Html->css('fatorcms/redactor10/redactor'); ?>
    <!--clock css-->
    <?php echo $this->Html->css('fatorcms/core/css3clock/style') ?>
    <!--Morris Chart CSS -->
    <?php //echo $this->Html->css('fatorcms/core/morris-chart/morris') ?>
    <!-- Custom styles for this template -->
    <?php echo $this->Html->css('fatorcms/style') ?>
    <?php echo $this->Html->css('fatorcms/style-responsive') ?>
    <?php echo $this->Html->css('fatorcms/core/iCheck/minimal') ?>
    <!-- css adicionais específicos -->
    <?php echo $this->Html->css('fatorcms/sweetalert2/sweetalert2.min') ?>
    <?php echo $this->Html->css('fatorcms/custom') ?>
    <?php echo $this->fetch('css') ?>


    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <?php echo $this->Html->script('fatorcms/ie8-responsive-file-warning/ie8-responsive-file-warning') ?>
    <![endif]-->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <?php echo $this->Html->script('fatorcms/core/jquery') ?>
</head>
<body>
    <section id="container">
        <?php echo $this->element('fatorcms/header') ?>
        <?php echo $this->element('fatorcms/sidebar') ?>

        <!--main content start-->
        <section id="main-content">
            <section class="wrapper">
                <?php echo $this->Session->flash('auth'); ?>
                <?php echo $this->Session->flash(); ?>

                <div class="row">
                    <div class="col-md-12">
                        <?php echo $this->Html->getCrumbList(
                            array('class' => 'breadcrumb', 'firstClass' => false, 'lastClass' => 'active'),
                            array(
                                'text' => 'Início',
                                'url'  => array(
                                    'plugin'     => 'fd_projetos',
                                    'controller' => 'fd_projetos',
                                    'action'     => 'index',
                                    'prefix'     => 'fatorcms'
                                )
                            )
                        ); ?>
                    </div>
                </div>

                <?php echo $this->fetch('content'); ?>
                <div class="row">
                    <div class="col-md-12"><?php echo $this->element('sql_dump'); ?></div>
                </div>
            </section>
        </section>
        <!--main content end-->

    </section>

    <!-- Placed js at the end of the document so the pages load faster -->
    <!--Core js-->
    <?php echo $this->Html->script('fatorcms/core/jquery-ui/jquery-ui-1.10.1.custom.min') ?>
    <?php echo $this->Html->script('fatorcms/core/bootstrap3/bootstrap.min') ?>
    <?php echo $this->Html->script('fatorcms/core/bootstrap-datepicker/bootstrap-datepicker') ?>
    <?php echo $this->Html->script('fatorcms/core/bootstrap-switch') ?>
    <?php echo $this->Html->script('fatorcms/core/jquery.dcjqaccordion.2.7') ?>
    <?php echo $this->Html->script('fatorcms/core/jquery.scrollTo.min') ?>
    <?php echo $this->Html->script('fatorcms/core/jQuery-slimScroll-1.3.0/jquery.slimscroll') ?>
    <?php echo $this->Html->script('fatorcms/core/jquery.nicescroll'); ?>
    <!--[if lte IE 8]><?php echo $this->Html->script('fatorcms/core/flot-chart/excanvas.min') ?><![endif]-->
    <?php echo $this->Html->script('fatorcms/core/skycons/skycons') ?>
    <?php echo $this->Html->script('fatorcms/core/jquery.scrollTo/jquery.scrollTo') ?>
    <?php //echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js') ?>
    <?php //echo $this->Html->script('fatorcms/core/calendar/clndr') ?>
    <?php //echo $this->Html->script('http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min.js') ?>
    <?php //echo $this->Html->script('fatorcms/core/calendar/moment-2.2.1') ?>
    <?php //echo $this->Html->script('fatorcms/core/evnt.calendar.init') ?>
    <?php //echo $this->Html->script('fatorcms/core/jvector-map/jquery-jvectormap-1.2.2.min') ?>
    <?php //echo $this->Html->script('fatorcms/core/jvector-map/jquery-jvectormap-us-lcc-en') ?>
    <?php echo $this->Html->script('fatorcms/core/gauge/gauge') ?>
    <!--clock init-->
    <?php echo $this->Html->script('fatorcms/core/css3clock/css3clock') ?>
    <!--Easy Pie Chart-->
    <?php //echo $this->Html->script('fatorcms/core/easypiechart/jquery.easypiechart') ?>
    <!--Sparkline Chart-->
    <?php //echo $this->Html->script('fatorcms/core/sparkline/jquery.sparkline') ?>
    <!--Morris Chart-->
    <?php //echo $this->Html->script('fatorcms/core/morris-chart/morris') ?>
    <?php //echo $this->Html->script('fatorcms/core/morris-chart/raphael-min') ?>

    <?php echo $this->Html->script('fatorcms/core/jquery.customSelect.min') ?>
    <!--common script init for all pages-->
    <?php //echo $this->Html->script('fatorcms/redactor/redactor'); ?>
    <?php echo $this->Html->script('fatorcms/sweetalert2/sweetalert2.min'); ?>
    <?php echo $this->Html->script('fatorcms/redactor10/redactor'); ?>
    <?php echo $this->Html->script('fatorcms/redactor10/plugins/table'); ?>
    <?php echo $this->Html->script('fatorcms/core/iCheck/jquery.icheck'); ?>
    <?php echo $this->Html->script('fatorcms/core/scripts') ?>
    <!--mask js-->
    <?php echo $this->Html->script('jquery.mask.min') ?>
    <?php echo $this->Html->script('fatorcms/maskmoney/maskmoney'); ?>
    <?php echo $this->Html->script('fatorcms/main') ?>
    <!--script for this page-->
    <!-- scripts adicionais específicos -->
    <?php echo $this->fetch('script') ?>
</body>
</html>