$(document).ready(function () {

    function timeOut(element) {
        setTimeout(function () {
            element.button('reset');
        }, 2500);
    }

    $('a.btn').click(function () {
        if ($(this).data("loading") != false) {
            $(this).attr('disabled', 'disabled').attr('data-loading-text', "<i class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></i> " + $(this).text()).button('loading');
            timeOut($(this));
        }
    });

    $('.deletar').click(function(e){
        e.preventDefault();
        var $this = $(this);
        swal({
            title: 'Deseja continuar?',
            text: "Você irá perder todos os imóveis relacionados ao projeto.",
            type: 'error',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Continuar'
        }).then(function(isConfirm) {
            if (isConfirm === true) {
                window.location.href = $this.attr('href');
            } else if (isConfirm === false) {
                swal(
                    'Ação cancelada',
                    'Não se preocupe o projeto não foi deletado.',
                    'info'
                );
            } else {
                swal(
                    'Ação cancelada',
                    'Não se preocupe o projeto não foi deletado.',
                    'info'
                );
            }
        })
    });


    $('.deleter').click(function(e){
        e.preventDefault();
        var $this = $(this);
        swal({
            title: 'Deseja continuar?',
            text: "Você irá perder todos os dados deste imóvel.",
            type: 'error',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Continuar'
        }).then(function(isConfirm) {
            if (isConfirm === true) {
                window.location.href = $this.attr('href');
            } else if (isConfirm === false) {
                swal(
                    'Ação cancelada',
                    'Não se preocupe o imóvel não foi deletado.',
                    'info'
                );
            } else {
                swal(
                    'Ação cancelada',
                    'Não se preocupe o imóvel não foi deletado.',
                    'info'
                );
            }
        })
    });

    $('form').submit(function () {
        if ($(this).data("loading") != false) {
            $(this).find('button[type="submit"]').attr('data-loading-text', "<i class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></i> Processando...").attr('disabled', 'disabled').button('loading');
            timeOut($(this).find('button[type="submit"]'));
        }
    });

    if ($('.no-limit').length) {
        $('.no-limit').click(function (e) {
            e.preventDefault();
            swal({
                title: 'Ops!',
                text: 'Seu limite de convites se esgotou, por favor, contate o administrador para aumentar seu limite.',
                type: 'info',
                confirmButtonText: 'Fechar'
            })
        });
    }

    if ($('.escolha-idioma').length > 0) {
        $(".escolha-idioma").tabs();
    }

    if ($('.data').length > 0) {
        $('.data').datepicker({
            dateFormat: 'dd/mm/yy'
        });
    }

    if ($('.editor').length > 0) {
        $('.editor').redactor({
            toolbarFixed: true,
            toolbarFixedTopOffset: 80, // pixels
            fileUpload: '/js/fatorcms/redactor10/phps/files.php',
            imageUpload: '/js/fatorcms/redactor10/phps/images.php',
            minHeight: 250,
            autoresize: true,
            focus: false,
            // cleanSpaces: false,
            paragraphize: false,
            // removeEmpty: ['p'],
            visual: true,
            linebreaks: false,
            pastePlainText: false,
            enterKey: true,
            cleanOnPaste: false,
            cleanStyleOnEnter: false,
            replaceDivs: false,
            removeAttr: [
                ['style']
            ],
            removeWithoutAttr: [],
            formatting: ['p', 'blockquote', 'h2', 'h3', 'a'],
            plugins: ['table'],
            formattingAdd: [
                {
                    tag: 'h3',
                    title: 'Sub-Título Cinza',
                    class: 'title-upper'
                },
                {
                    tag: 'h4',
                    title: 'Sub-Título Vermelho',
                    class: 'title-color title-red'
                },
                // {
                //     tag: 'div',
                //     title: 'Box Vermelho com PDF',
                //     class: 'my-pdf-big'
                // },
                {
                    tag: 'ul',
                    title: 'Lista com pontos vermlehos',
                    class: 'ul-style'
                },
                {
                    tag: 'a',
                    title: 'Link Vermelho com PDF',
                    class: 'link-my-pdf-big'
                },
                {
                    tag: 'a',
                    title: 'Link Vermelho com XLS',
                    class: 'link-my-xls-big'
                },
                {
                    tag: 'a',
                    title: 'Link Vermelho com DOC',
                    class: 'link-my-doc-big'
                },
            ]
        });
    }

    $('#limit').change(function () {
        if ($(this).val() != "") {
            window.location = $(this).val();
        } else {
            window.location = $('#limit').attr('rel').replace(/\/limit:(.*)/gi, '');
        }
    });

    setMasks();
});

function setMasks() {
    $('input.date').mask('11/11/1111');
    $('input.time').mask('00:00:00');
    $('input.date_time').mask('99/99/9999 00:00:00');
    $('input.cep').mask('99999-999');
    $('input.cpf').mask('999.999.999-99');
    $('input.cnpj').mask('99.999.999.9999/99');
    $('input.phone').mask('9999-99999');
    $('input.phone_with_ddd').mask('(99) 9999-9999');
    $('input.card_credit').mask('9999999999999999');
    $('input.code_validate').mask('999');
    $('input.number').mask('99999999999999999999999999#');
    $('input.money1').mask('000.000.000.000.000,00', {reverse: true});
}