$(document).ready(function () {

    var $modalVenda = $('#vendido');

    var $status = $('#ImovelStatus');
    var $statusValue = $status.val();

    $("#ImovelValor").maskMoney({
        thousands: '.',
        decimal: ',',
        allowZero: false,
        suffix: ''
    });

    $status.change(function(e) {
        e.preventDefault();
        if ($(this).val() == 3) {
            $('#vendido').modal('show');
        }
    });

    $modalVenda.on('show.bs.modal', function () {
        $('.sell').attr('required', 'required');
    });
    $modalVenda.on('hide.bs.modal', function () {
        $('.sell').removeAttr('required');
        $status.val($statusValue);
    });
});