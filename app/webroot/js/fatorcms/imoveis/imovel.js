$(document).ready(function () {

    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy'
    });

    $('#redactor').redactor();


    $('#convite').on('show.bs.modal', function (e) {
        var $this = $(this);
        var $data = $this.data('bs.modal');
        if ($data.options.id) {
            var $form = $('#formConvite');
            $form.attr('action', $form.attr('action') + $data.options.id);
        }
    });

    $('#reservar').on('show.bs.modal', function(e) {
        var $this = $(this);
        var $data = $this.data('bs.modal');
        if ($data.options.id) {
            var $form = $('#formReserva');
            $form.attr('action', $data.options.id);
        }
    });

    $('#vendido').on('show.bs.modal', function(e) {
        var $this = $(this);
        var $data = $this.data('bs.modal');
        if ($data.options.id) {
            var $form = $('#formVendido');
            $form.attr('action', $data.options.id);
        }
    });

    if ($('.dataTables-example').length) {
        var table = $('.dataTables-example').DataTable({
            "aaSorting": [],
            "responsive": true,
            "bSort": false,
            "dom": '<"html5buttons"B>lTfgitp',
            "bLengthChange": true,
            "lengthMenu": [[20, 40, 80, -1], [20, 40, 80, "All"]],
            "bInfo": false,
            buttons: [
                {
                    extend: 'copy',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'csv', title: 'csv-imoveis',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'excel', title: 'excel-imoveis',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'pdf', title: 'pdf-imoveis',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    },
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                'colvis'
            ],
            "language": {
                "search": "Procurar:",
                "lengthMenu": "mostrar _MENU_ registros por página",
                "infoEmpty": "Nenhum registro encontrado",
                "paginate": {
                    "next": "Próximo",
                    "previous": "Anterior"
                },
                "buttons": {
                    colvis: 'EDITAR COLUNAS',
                    print: "IMPRIMIR",
                    copy: "COPIAR"
                }
            },
            "colReorder": true
        });
    }
});
