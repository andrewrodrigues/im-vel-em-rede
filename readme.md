![FatorCMS](http://www.fatordigital.com.br/logo_fatorcms_234x55.png "FatorCMS")

-------
# Framework: CakePHP 2.4.0

Protótipo do FatorCMS em Cake. A ideia é manter aqui a base do CMS dos sites institucionais.

# Plugins:
- Acl
- AclExtras
- FdBanners
- FdBlocos
- FdDashboard
- FdEmails
- FdGalerias
- FdLogs
- FdMenus
- FdNoticias
- FdPaginas
- FdRotas
- FdSac
- FdSettings
- FdSvn
- FdUsuarios
- FilterResults
- PhpExcel
- Upload

# Libraries
- Error / AppExceptionRenderer
- Wideimage
- CustomUploadHandler
- GaleriaUploadHandler

